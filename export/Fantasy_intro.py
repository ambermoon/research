#!/usr/bin/env python3

import array
import copy
import ctypes
import datetime
import png
import sys

# All offsets are for the deploded v1.04+ publisher intro.
FANTASY_INTRO_FILE = '../data/dh/v104-1993-11-18-german/Amberfiles/Fantasy_intro'
# All images have 5 interlaced bitplanes (32 colors).
BCKGRND_IMAGE_SEEK = 0x00E924 # 320x256
WRITING_IMAGE_SEEK = 0x01B994 # 208x83
WRITING_SPARK_SEEK = 0x01B124 # 16*2x9*12
FAIRY_SPARK_2_SEEK = 0x001618 # 16*2x5*3
# Eight (four attached) sprites per image.
FAIRY_SPRITES_SEEK = 0x001744 # 16*4*2x71*23
# List of positions (with two x == 12345 tags).
FAIRY_MOVEPOS_SEEK = 0x01E3BA # int16[761][2]

BITPLANE_COUNT = 5
PALETTE_LENGTH = 1 << BITPLANE_COUNT

COLOR4_TO_COLOR8 = (1 << 4) | 1 # 0x11 * 0x0F == 0xFF

# Ripped tables from the deploded v1.04+ publisher intro.
UPPER_PALETTE_RGB = ( # 0x0015D8, screen line 000-133, *= COLOR4_TO_COLOR8
    (  0,   0,   0), (  0,   0, 102), (  0,   0,  68), ( 68,  68, 204),
    (  0,   0, 136), ( 34,  34, 170), ( 51,  34,  85), ( 51,  17,  68),
    ( 34,   0,  51), (  0,  68,   0), (  0, 102,  68), (  0, 136, 119),
    (  0, 170, 170), (119, 204, 221), (238, 238, 238), (102, 102, 204),
    (204, 204, 238), (170, 170, 238), (136, 136, 238), ( 34,  34,  34),
    (221, 221,  68), (204, 153,   0), ( 68,   0,  17), (153,  34,   0),
    ( 68,  68,  68), (102, 102, 102), (136, 136, 136), (170, 170, 170),
    (204, 204, 204), (119,   0,  17), (187, 119,  17), (170,  68,  17))
LOWER_PALETTE_RGB = ( # 0x001598, screen line 134-255, *= COLOR4_TO_COLOR8
    *UPPER_PALETTE_RGB[0:20],
    ( 51,  51,  51), ( 85,  85, 170), (  0,   0,   0), (170, 119, 119),
    *UPPER_PALETTE_RGB[24:29],
    (153,  85,  85), (119,  51,  34), (102,  34,   0))
WRITING_SPARK_DRAW_IMAGE = ( # 0x000470, int16[8][0x20]
    6, 3, 0, 3, 6, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    4, 1, 4, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    8, 5, 2, 5, 8, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    6, 6, 3, 3, 0, 0, 3, 3, 3, 6, 6, 6, -1, 0, 0, 0,
    7, 7, 4, 4, 0, 0, 0, 4, 4, 7, 7, -1, 0, 0, 0, 0,
    8, 8, 8, 5, 5, 5, 0, 0, 0, 5, 5, 5, 8, 8, 8, -1,
    3, 0, 3, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    5, 0, 5, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
FAIRY_SPARK_1_DRAW_COLOR = ( # 0x0006BA, >>= 2 (function offset to color)
    9, 11, 13, 14, 13, 12, 12, 11, 10, 10, 9, 9, 10, 14, 12, 10,
    26, 28, 24, 26, 28, 14, 27, 26, 25, 14, 20, 9, -1)
FAIRY_SPARK_2_DRAW_COLOR = ( # 0x00077A, -image /= 100 (offset to number)
    9, 10, 11, 12, 11, 10, 9, 9, 9, 11, 14, 13, 12, 11, 10, 0,
    9, 10, 11, 12, 11, 10, 9, 9, 9, 11, 14, -1, -2, -3, 0)
FAIRY_ANIM_MAGIC_WAND_POS = ( # 0x0011B4, x -= 125, y -= 70
    (38, 13), (40, 14), (41, 13), (43, 14), (45, 16), (41, 13), (39, 14),
    (37, 14), (38, 17), (44, 16), (53, 22), (56, 28), (58, 33), (58, 36),
    (58, 37), (55, 30), (54, 29), (50, 23), (45, 18))


BUFFER_WIDTH = 320
BUFFER_HEIGHT = 256

class DrawBuffer:
    def __init__(self):
        self.back = tuple(array.array('B', [0] * BUFFER_WIDTH) for y in range(BUFFER_HEIGHT))
        self.front = tuple(array.array('B', [0] * BUFFER_WIDTH) for y in range(BUFFER_HEIGHT))
    def swap(self):
        self.front, self.back = self.back, self.front

buffer = DrawBuffer()


class Image:
    OPAQUE, MASKED, SPRITE = range(3)
    def __init__(self, mode, width, height):
        self.mode = mode
        self.width = width
        self.height = height
        self.image = tuple(array.array('B', [0] * width) for y in range(height))
    def __getitem__(self, y):
        return self.image[y]
    def __len__(self):
        return self.height
    def get_blit_range(self, x, y, w, h, d):
        return (
            range(max(0, x), min(x + w, len(d[0]))),
            range(max(0, y), min(y + h, len(d))))
    def blit(self, x, y, d):
        if self.mode is Image.MASKED:
            return self.blit_masked(x, y, d)
        if self.mode is Image.SPRITE:
            return self.blit_sprite(x, y, d)
        return self.blit_opage(x, y, d)
    def blit_opage(self, x, y, d):
        dxr, dyr = self.get_blit_range(x, y, self.width, self.height, d)
        dxs = slice(dxr.start, dxr.stop)
        axs = slice(dxs.start - x, dxs.stop - x)
        for dy in dyr:
            d[dy][dxs] = self.image[dy - y][axs]
    def blit_masked(self, x, y, d):
        w = self.width // 2 # colors in left half, masks in right
        dxr, dyr = self.get_blit_range(x, y, w, self.height, d)
        for dy in dyr:
            dr = d[dy]
            ar = self.image[dy - y]
            for dx in dxr:
                ax = dx - x
                m = ar[ax + w]
                dr[dx] &= ~m
                dr[dx] |= m & ar[ax]
    def blit_sprite(self, x, y, d):
        dxr, dyr = self.get_blit_range(x, y, self.width, self.height, d)
        for dy in dyr:
            dr = d[dy]
            ar = self.image[dy - y]
            for dx in dxr:
                c = ar[dx - x]
                if c > 16:
                    dr[dx] = c

class AmigaStruct(ctypes.BigEndianStructure):
    def clear(self):
        ctypes.memset(ctypes.addressof(self), 0, ctypes.sizeof(self))
        return self
    @classmethod
    def from_rawio(cls, b):
        c = cls()
        if b.readinto(c) != ctypes.sizeof(cls):
            raise Exception(f'failed to read {cls.__name__} structure')
        return c
    @classmethod
    def from_file(cls, offset):
        with open(FANTASY_INTRO_FILE, 'rb') as b:
            b.seek(offset)
            return cls.from_rawio(b)

def load_image_strip(mode, width, height, count, offset):
    LINE_LEN = (width + 15) // 16
    class PlanarInterlacedImage(AmigaStruct):
        _fields_ = [
            ('image', ctypes.c_uint16 * LINE_LEN * BITPLANE_COUNT * height)]
        def __getitem__(self, x_y):
            x, y = x_y
            w, b = divmod(x, 16)
            m = 0x8000 >> b
            c = 0
            r = self.image[y]
            for p in range(BITPLANE_COUNT):
                if r[p][w] & m:
                    c |= 1 << p
            return c
    strip = list()
    for n in range(count):
        planar = PlanarInterlacedImage.from_file(offset)
        offset += ctypes.sizeof(planar)
        chunky = Image(mode, width, height)
        for y in range(height):
            for x in range(width):
                chunky.image[y][x] = planar[x, y]
        strip.append(chunky)
    return tuple(strip)

def load_image(mode, width, height, offset):
    return load_image_strip(mode, width, height, 1, offset)[0]

# note that the background image is updated during the intro (writing)
bckgrnd_image = load_image(Image.OPAQUE, 320, 256, BCKGRND_IMAGE_SEEK)
WRITING_IMAGE = load_image(Image.OPAQUE, 208,  83, WRITING_IMAGE_SEEK)
WRITING_SPARK_IMAGE = load_image_strip(Image.MASKED, 16 * 2, 9, 12, WRITING_SPARK_SEEK)
FAIRY_SPARK_2_IMAGE = load_image_strip(Image.MASKED, 16 * 2, 5,  3, FAIRY_SPARK_2_SEEK)

def load_sprite_strip(width, height, attached, count, offset):
    class AmigaSpriteData(AmigaStruct):
        _fields_ = [
            ('ctrl', ctypes.c_uint16 * 2),
            ('data', ctypes.c_uint16 * 2 * height),
            ('next', ctypes.c_uint16 * 2)]
        def __getitem__(self, x_y):
            x, y = x_y
            x = 0x8000 >> x
            c = 0
            for p in range(2):
                if self.data[y][p] & x:
                    c |= 1 << p
            return c
    DATA_COUNT = 1 if not attached else 2
    TILE_COUNT = (width + 15) // 16
    class AmigaSpriteImage(AmigaStruct):
        _fields_ = [
            ('image', AmigaSpriteData * DATA_COUNT * TILE_COUNT)]
        def __getitem__(self, x_y):
            x, y = x_y
            t, x = divmod(x, 16)
            t = self.image[t]
            c = 0
            for d in reversed(range(DATA_COUNT)):
                c = (c << 2) | t[d][x, y]
            return c + 16
    strip = list()
    for n in range(count):
        sprite = AmigaSpriteImage.from_file(offset)
        offset += ctypes.sizeof(sprite)
        chunky = Image(Image.SPRITE, width, height)
        for y in range(height):
            for x in range(width):
                chunky.image[y][x] = sprite[x, y]
        strip.append(chunky)
    return tuple(strip)

FAIRY_SPRITE_IMAGE = load_sprite_strip(16 * 4, 71, True, 23, FAIRY_SPRITES_SEEK)


class AmigaPos16(AmigaStruct):
    _fields_ = [
        ('x', ctypes.c_int16),
        ('y', ctypes.c_int16)]

class FairyMovePosArray(AmigaStruct):
    _fields_ = [
        ('pos', AmigaPos16 * 761)]
    def __getitem__(self, i):
        return self.pos[i]

FAIRY_MOVEPOS_ARRAY = FairyMovePosArray.from_file(FAIRY_MOVEPOS_SEEK)


class IntroState:
    def __init__(self):
        self.writing_pos_x = -1
        self.vblank_counter = 0
        self.vblank_active = False
        self.sprite_anim_mode = 0
        self.sprite_pos_index = 0
        self.sprite_pos = FAIRY_MOVEPOS_ARRAY[self.sprite_pos_index]
        self.sprite_image_delay = 0
        self.sprite_image_index = 0
        self.sprite_spark_1_delay = 0
        self.sprite_anim_spark_pos_index = None
        self.sprite_anim_spark_pos_x = 0
        self.sprite_anim_spark_pos_y = 0
        self.screen_palette_fade_out = 0
        self.field_counter = 0
        self.screen_sprite_pos = None
        self.screen_sprite_img = None
    def next_sprite_pos(self):
        self.sprite_pos_index += 1
        self.sprite_pos = FAIRY_MOVEPOS_ARRAY[self.sprite_pos_index]
        return self.sprite_pos
    def prev_sprite_pos(self):
        self.sprite_pos_index -= 1
        self.sprite_pos = FAIRY_MOVEPOS_ARRAY[self.sprite_pos_index]
        return self.sprite_pos

intro = IntroState()


# This palette has been generated from the upper/lower palette colors.
SCREEN_COLOR_RGB = (
    # upper palette (32)
    *UPPER_PALETTE_RGB[0:32],
    # + lower palette (32 - 26 = 6)
    *LOWER_PALETTE_RGB[20:22],
    *LOWER_PALETTE_RGB[23:24],
    *LOWER_PALETTE_RGB[29:32],
    # + background fade-in (114 - 28 = 86)
    (  0,   0,  17), (  0,   0,  34), (  0,   0,  51), (  0,   0,  85),
    (  0,   0, 119), ( 17,   0,  17), ( 17,  17,   0), ( 17,  17,  17),
    ( 34,   0,  17), ( 34,   0,  34), ( 34,  17,  34), ( 34,  34,   0),
    ( 34,  34,  17), ( 34,  34,  51), ( 34,  34,  68), ( 34,  34,  85),
    ( 34,  34, 102), ( 34,  34, 119), ( 34,  34, 136), ( 34,  34, 153),
    ( 51,   0,  17), ( 51,  17,  51), ( 51,  34,   0), ( 51,  34,  51),
    ( 51,  34,  68), ( 51,  51,   0), ( 51,  51,  17), ( 68,  34,   0),
    ( 68,  68,   0), ( 68,  68,  17), ( 68,  68,  85), ( 68,  68, 102),
    ( 68,  68, 119), ( 68,  68, 136), ( 68,  68, 153), ( 68,  68, 170),
    ( 68,  68, 187), ( 85,   0,  17), ( 85,  34,   0), ( 85,  68,  17),
    ( 85,  85,   0), ( 85,  85,  17), ( 85,  85,  68), ( 85,  85,  85),
    (102,   0,  17), (102,  68,  17), (102, 102,   0), (102, 102,  17),
    (102, 102,  68), (102, 102, 119), (102, 102, 136), (102, 102, 153),
    (102, 102, 170), (102, 102, 187), (119,  34,   0), (119,  68,  17),
    (119, 119,   0), (119, 119,  17), (119, 119,  68), (119, 119, 119),
    (136,  34,   0), (136,  68,  17), (136, 119,  17), (136, 136,   0),
    (136, 136,  68), (136, 136, 153), (136, 136, 170), (136, 136, 187),
    (136, 136, 204), (136, 136, 221), (153,  68,  17), (153, 119,  17),
    (153, 153,   0), (153, 153,  68), (153, 153, 153), (170, 119,  17),
    (170, 153,   0), (170, 170,  68), (170, 170, 187), (170, 170, 204),
    (170, 170, 221), (187, 153,   0), (187, 187,  68), (187, 187, 187),
    (204, 204,  68), (204, 204, 221),
    # + background/writing fade-out (93 - 47 = 46)
    ( 17,   0,   0), ( 17,   0,  34), ( 17,   0,  51), ( 17,  17,  51),
    ( 17,  17,  85), ( 17,  17, 119), ( 17,  17, 153), ( 34,   0,   0),
    ( 34,  17,  68), ( 51,   0,   0), ( 51,  51,  85), ( 51,  51, 119),
    ( 51,  51, 153), ( 51,  51, 187), ( 68,   0,   0), ( 68,  17,   0),
    ( 85,   0,   0), ( 85,  17,   0), ( 85,  85, 119), ( 85,  85, 153),
    ( 85,  85, 187), (102,   0,   0), (102,  51,   0), (119,   0,   0),
    (119,  17,   0), (119,  51,   0), (119,  68,   0), (119, 119, 153),
    (119, 119, 187), (119, 119, 221), (136,  17,   0), (136,  68,   0),
    (136,  85,   0), (153,  51,   0), (153,  85,   0), (153, 102,   0),
    (153, 153, 187), (153, 153, 221), (170, 102,   0), (170, 119,   0),
    (170, 170,  17), (187, 136,   0), (187, 187,  34), (187, 187, 221),
    (204, 204,  51), (221, 221, 221),
    # + complete (skippable intro) fade-out (140 - 95 = 45)
    (  0,  17,   0), (  0,  17,  17), (  0,  17,  34), (  0,  34,   0),
    (  0,  34,  17), (  0,  34,  34), (  0,  34,  51), (  0,  51,   0),
    (  0,  51,  17), (  0,  51,  34), (  0,  51,  51), (  0,  51,  68),
    (  0,  68,  34), (  0,  68,  51), (  0,  68,  68), (  0,  68,  85),
    (  0,  85,  51), (  0,  85,  68), (  0,  85,  85), (  0,  85, 102),
    (  0, 102,  85), (  0, 102, 102), (  0, 119, 102), (  0, 119, 119),
    (  0, 136, 136), (  0, 153, 153), ( 17,  17, 102), ( 17, 102, 119),
    ( 34, 119, 136), ( 51,  51, 136), ( 51, 136, 153), ( 68,  17,  17),
    ( 68, 153, 170), ( 85,  17,  17), ( 85,  34,  34), ( 85, 170, 187),
    (102,  34,  17), (102,  34,  34), (102,  51,  51), (102, 187, 204),
    (119,  51,  51), (119,  68,  68), (136,  68,  68), (136,  85,  85),
    (153, 102, 102),
    # 215 + complete (all base colors) fade-in (156 - 141 = 15)
    ( 51,  51,  34), ( 68,  51,  34), ( 85,  51,  34), ( 85,  85, 102),
    ( 85,  85, 136), (102,  51,  34), (102,  85,  85), (119,  85,  85),
    (119, 136, 136), (119, 153, 153), (119, 170, 170), (119, 187, 187),
    (119, 204, 204), (136, 119, 119), (153, 119, 119)
)

SCREEN_BLACK_COLOR = SCREEN_COLOR_RGB.index((0, 0, 0))
SCREEN_COLOR_SLICE = slice(32 + 6 + 86 + 46) # full unskipped intro

# Start with black screen.
screen_upper_palette = tuple(SCREEN_BLACK_COLOR for c in range(PALETTE_LENGTH))
screen_lower_palette = tuple(SCREEN_BLACK_COLOR for c in range(PALETTE_LENGTH))

def fade_in():
    global screen_upper_palette
    global screen_lower_palette
    def fade(pal_map, pal_rgb):
        m = list(pal_map)
        for i in range(len(m)):
            mc = list(SCREEN_COLOR_RGB[m[i]])
            pc = pal_rgb[i]
            for c in range(len(mc)):
               if mc[c] < pc[c]:
                   mc[c] += COLOR4_TO_COLOR8
            m[i] = SCREEN_COLOR_RGB.index(tuple(mc))
        return tuple(m)
    screen_upper_palette = fade(screen_upper_palette, UPPER_PALETTE_RGB)
    screen_lower_palette = fade(screen_lower_palette, LOWER_PALETTE_RGB)

def fade_out():
    global screen_upper_palette
    global screen_lower_palette
    def fade(pal_map):
        m = list(pal_map)
        for i in range(len(m)):
            mc = list(SCREEN_COLOR_RGB[m[i]])
            for c in range(len(mc)):
               if mc[c] > 0:
                   mc[c] -= COLOR4_TO_COLOR8
            m[i] = SCREEN_COLOR_RGB.index(tuple(mc))
        return tuple(m)
    screen_upper_palette = fade(screen_upper_palette)
    screen_lower_palette = fade(screen_lower_palette)

SCREEN_WIDTH = BUFFER_WIDTH * 2
SCREEN_HEIGHT = BUFFER_HEIGHT * 2

screen_field = 0
screen_frame = tuple(array.array('B', [0] * SCREEN_WIDTH) for y in range(SCREEN_HEIGHT))

def draw_field():
    global screen_field
    screen_buffer = copy.deepcopy(buffer.front)
    sprite_pos = intro.screen_sprite_pos
    if sprite_pos is not None:
        FAIRY_SPRITE_IMAGE[intro.screen_sprite_img].blit(
            sprite_pos.x, sprite_pos.y, screen_buffer)
    p = screen_upper_palette
    for y in range(screen_field, SCREEN_HEIGHT, 2):
        i = y // 2
        if i == 134:
            p = screen_lower_palette
        ar = screen_buffer[i]
        dr = screen_frame[y]
        for x in range(0, SCREEN_WIDTH, 2):
            c = p[ar[x // 2]]
            dr[x] = c
            dr[x + 1] = c
    screen_field ^= 1

def dump_screen(file):
    with open(file, 'wb') as f:
        png.Writer(
            width=SCREEN_WIDTH, height=SCREEN_HEIGHT,
            bitdepth=8, palette=SCREEN_COLOR_RGB[SCREEN_COLOR_SLICE],
            compression=9).write(f, screen_frame)


def random_generator():
    state = 17
    while True:
        state = (state & 0xFFFF) * 53527
        yield (state >> 8) & 0x7FFF

random_iterator = random_generator()

def next_random():
    return next(random_iterator)


class FairySpark:
    def __init__(self):
        self.x = -1
        self.y_16th = 0
        self.y_step = 0
        self.index = 0
    def activate(self, x, y, step, index): # step is one sixteenth of a line
        self.x = x
        self.y_16th = y << 4
        self.y_step = step
        self.index = index
    @property
    def active(self):
        return self.x >= 0
    @property
    def y(self):
        return self.y_16th >> 4
    def deactivate(self):
        self.x = -1
    def next_step(self):
        if self.active:
            self.y_16th += self.y_step
            if self.y_16th >= (256 << 4):
                self.deactivate()
        return self.active

fairy_spark_1 = tuple(FairySpark() for n in range(96))
fairy_spark_2 = tuple(FairySpark() for n in range(512))

def add_fairy_spark_1(x, y):
    if (0 <= x) and (x <= 320 - 1 - 8) and (134 <= y) and (y <= 256 - 1):
        for s in fairy_spark_1:
            if not s.active:
                _ = next_random()
                step = (2 << 4) + (_ & 0x0007)
                _ = next_random()
                index = _ % 11
                s.activate(x, y, step, index)
                break

def add_fairy_spark_2(x1, y1):
    for s2 in fairy_spark_2:
        if not s2.active:
            _ = next_random()
            x2 = x1 + ((_ & 0x0060) >> 5)
            step2 = ((1 << 4) >> 1) + (_ & 0x000F)
            index2 = 0 if (_ & 0x0E00) != 0 else 16
            s2.activate(x2, y1, step2, index2)
            break


def draw_fairy_spark_1(buffer):
    _ = next_random()
    spawn_delay = _ & 0x0003
    for s1 in fairy_spark_1:
        if s1.next_step():
            c1 = FAIRY_SPARK_1_DRAW_COLOR[s1.index]
            if c1 < 0:
                s1.index = 0
                c1 = FAIRY_SPARK_1_DRAW_COLOR[0]
            s1.index += 1
            buffer[s1.y][s1.x] = c1
            buffer[s1.y][s1.x + 1] = c1
            spawn_delay -= 1
            if spawn_delay < 0:
                spawn_delay += 4 - 1
                add_fairy_spark_2(s1.x, s1.y)

def draw_fairy_spark_2(buffer):
    for s2 in fairy_spark_2:
        if s2.next_step():
            c2 = FAIRY_SPARK_2_DRAW_COLOR[s2.index]
            if c2 == 0:
                s2.deactivate()
                continue
            s2.index += 1
            if c2 < 0:
                if s2.y <= 256 - 1 - 5:
                    FAIRY_SPARK_2_IMAGE[-1 - c2].blit(s2.x, s2.y, buffer)
            else:
                buffer[s2.y][s2.x] = c2

class WritingSpark:
    def __init__(self):
        self.x = -1
        self.y = 0
        self.index = 0
    def activate(self, x, y, index):
        self.x = x
        self.y = y
        self.index = index
    @property
    def active(self):
        return self.x >= 0
    def deactivate(self):
        self.x = -1

writing_spark = tuple(WritingSpark() for n in range(400))

def add_writing_spark(x, y, index):
    for s in writing_spark:
        if not s.active:
            s.activate(x, y, index)
            break

def draw_writing(buffer):
    if intro.writing_pos_x >= 0:
        if (64 <= intro.writing_pos_x) and (intro.writing_pos_x < 272):
            for y in range(WRITING_IMAGE.height):
                dxs = slice(intro.writing_pos_x, intro.writing_pos_x + 4)
                axs = slice(intro.writing_pos_x - 64, intro.writing_pos_x - 64 + 4)
                bckgrnd_image[146 + y][dxs] = WRITING_IMAGE[y][axs]
        for n in range(24):
            _ = next_random()
            y = 140 + (_ % 88)
            _ = next_random()
            x = intro.writing_pos_x + (_ & 0x0007) - 4
            index = (_ & 0x00E0) >> 1
            add_writing_spark(x, y, index)
        intro.writing_pos_x += 4
        if intro.writing_pos_x >= 276:
            intro.writing_pos_x = -1
            intro.sprite_anim_mode = 0
    for s in writing_spark:
        if s.active:
            i = WRITING_SPARK_DRAW_IMAGE[s.index]
            if i < 0:
                s.deactivate()
            else:
                s.index += 1
                WRITING_SPARK_IMAGE[i].blit(s.x, s.y, buffer)


def wait_vblank():
    intro.field_counter += 1
    draw_field()
    dump_screen(f'./Fantasy_intro/Fantasy_intro-{intro.field_counter:04d}.png')

    intro.vblank_counter += 1
    if not intro.vblank_active:
        return
    if intro.sprite_anim_mode == 0:
        if intro.next_sprite_pos().x == 12345:
            if intro.sprite_pos.y >= 0:
                intro.next_sprite_pos()
                intro.sprite_anim_mode = -1
                intro.sprite_anim_spark_pos_index = -1
            else:
                intro.prev_sprite_pos()
                if intro.screen_palette_fade_out == 0:
                    intro.screen_palette_fade_out = 133
    intro.screen_sprite_pos = copy.deepcopy(intro.sprite_pos)
    intro.screen_sprite_img = copy.deepcopy(intro.sprite_image_index)
    intro.sprite_image_delay -= 1
    if intro.sprite_image_delay < 0:
        intro.sprite_image_delay += 4 - 1
        intro.sprite_image_index += 1
        if intro.sprite_image_index == 4:
            intro.sprite_image_index = 0
        if intro.sprite_image_index == 23:
            intro.sprite_image_index = 0
            intro.sprite_anim_mode = 1
            intro.writing_pos_x = -110
        if intro.sprite_anim_mode < 0:
            if intro.sprite_anim_spark_pos_index >= 0:
                intro.sprite_anim_spark_pos_index += 1
            if intro.sprite_image_index == 3:
                intro.sprite_image_index = 4
                intro.sprite_anim_spark_pos_index = 0
    intro.sprite_spark_1_delay -= 1
    if intro.sprite_spark_1_delay < 0:
        if intro.sprite_anim_mode == 0:
            intro.sprite_spark_1_delay += 3 - 1
        else:
            intro.sprite_spark_1_delay += 33 - 1
        add_fairy_spark_1(intro.sprite_pos.x + 19, intro.sprite_pos.y + 17) # wing joints
    if (intro.sprite_anim_mode < 0) and (intro.sprite_anim_spark_pos_index >= 0) and (intro.sprite_image_delay == 0):
        magic_wand_pos = FAIRY_ANIM_MAGIC_WAND_POS[intro.sprite_anim_spark_pos_index]
        x = intro.sprite_pos.x + magic_wand_pos[0]
        y = intro.sprite_pos.y + magic_wand_pos[1]
        if intro.sprite_image_index == 12 + 4:
            intro.sprite_anim_spark_pos_x = x
            intro.sprite_anim_spark_pos_y = y
        add_fairy_spark_1(x, y)
    if intro.sprite_anim_spark_pos_x >= 0:
        for n in range(6):
            _ = next_random()
            x = intro.sprite_anim_spark_pos_x + ((_ & 0x000F) >> 0)
            y = intro.sprite_anim_spark_pos_y + ((_ & 0x00F0) >> 4)
            add_fairy_spark_1(x, y)
        intro.sprite_anim_spark_pos_x += 6
        intro.sprite_anim_spark_pos_y += 1
        if intro.sprite_anim_spark_pos_x >= 160:
            intro.sprite_anim_spark_pos_x = -1
    if intro.writing_pos_x < -1:
        intro.writing_pos_x += 1
        if intro.writing_pos_x == -1:
            intro.writing_pos_x = 60

def intro_main_loop():
    while True:
        buffer.swap()
        wait_vblank()
        intro.vblank_counter = 0
        bckgrnd_image.blit(0, 0, buffer.back) # original y 134+
        draw_fairy_spark_1(buffer.back)
        draw_fairy_spark_2(buffer.back)
        draw_writing(buffer.back)
        while intro.vblank_counter < 2:
            wait_vblank()
        if intro.screen_palette_fade_out != 0:
            break

def intro_main():
    wait_vblank()
    bckgrnd_image.blit(0, 0, buffer.back)
    bckgrnd_image.blit(0, 0, buffer.front)
    wait_vblank()
    for n in range(15):
        wait_vblank()
        wait_vblank()
        fade_in()
    wait_vblank()
    intro.vblank_active = True
    while True:
        intro_main_loop()
        intro.screen_palette_fade_out -= 1
        if intro.screen_palette_fade_out <= 0:
            break
        if intro.screen_palette_fade_out < 16:
            fade_out()


def main(argv):
    print(datetime.datetime.now())
    intro_main()
    print(datetime.datetime.now())
    print(f'fields: {intro.field_counter}')

if __name__ == '__main__':
    main(sys.argv)

