Welcome to the development corner of the Ambermoon research project.

Your help is very welcome :)

Please respect the project's [CLangFormat style](.clang-format).
It is recommended to enable automatic code formatting in your
development environment.

The code is intended to be used with a
[C99](https://en.wikipedia.org/wiki/C99) compiler.
You should try to support older compilers if possible
and should avoid C99-only features that cannot be used
in C++ (one parameter as array size of another parameter).

`int` and `size_t` are expected to have at least 32 bits,
because enumerations are used for constants
(instead of preprocessor macros), and
some game files are larger than 64 KiB.


Please try to follow the, somewhat unusual, code style
(e.g. upper camel case functions, types and constants).

