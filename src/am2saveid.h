#ifndef AM2SAVEID_H
#define AM2SAVEID_H

#include "am2diskid.h"
#include "am2fileid.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief  Ambermoon save file identifier.
 */
enum AM2SaveId {
  AM2SaveId_None,         /*!< Invalid/no save identifier */
  AM2SaveId_PartyData,    /*!< AMBER_J:Save.??/Party_data.sav */
  AM2SaveId_PartyChar,    /*!< AMBER_J:Save.??/Party_char.amb */
  AM2SaveId_Automap,      /*!< AMBER_J:Save.??/Automap.amb */
  AM2SaveId_ChestData,    /*!< AMBER_J:Save.??/Chest_data.amb */
  AM2SaveId_MerchantData, /*!< AMBER_J:Save.??/Merchant_data.amb */
  AM2_SAVE_ID_COUNT
};

/*!
 * \brief  Save file to game file mapping.
 */
extern enum AM2FileId const AM2SaveFileId[AM2_SAVE_ID_COUNT];

/*!
 * \brief  Saved game slot number.
 */
enum AM2SaveSlot {
  /*!
   * \brief  Slot number for starting a new game.
   */
  AM2SaveSlot_None = 0,
  /*!
   * \brief  Minimum slot number for a saved game.
   */
  AM2_SAVE_SLOT_MIN = 1,
  /*!
   * \brief  Maximum slot number for a saved game.
   */
  AM2_SAVE_SLOT_MAX = 10
};

/*!
 * \brief  Get the full file path for a save file by id.
 * \param[out]  path  Pointer to the character array to write to.
 * \param[in]   mode  Ambermoon disk mode.  \sa  #AM2DiskMode
 * \param[in]   save  Save identifier.  \sa  #AM2SaveId
 * \param[in]   slot  Saved game slot number.  \sa  #AM2SaveSlot
 * \return            Returns a pointer to the start of the relative file
 *                    name (in path), or \c NULL for invalid parameters.
 * \note  The returned string is an AmigaOS path, where the root directory
 *        (drive) separator is ":", and the directory separator is "/".
 */
char *AM2GetSaveIdPath(char path[AM2_PATH_MAX], enum AM2DiskMode mode,
                       enum AM2SaveId save, enum AM2SaveSlot slot);

#ifdef __cplusplus
}
#endif

#endif
