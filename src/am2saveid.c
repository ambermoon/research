#include "am2saveid.h"
#include <string.h>

enum AM2FileId const AM2SaveFileId[AM2_SAVE_ID_COUNT] = {
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
#define _i(S, F) [S] = F
#else
#define _i(S, F) F
#endif
    _i(AM2SaveId_None, AM2FileId_None),
    _i(AM2SaveId_PartyData, AM2FileId_PartyData),
    _i(AM2SaveId_PartyChar, AM2FileId_PartyChar),
    _i(AM2SaveId_Automap, AM2FileId_Automap),
    _i(AM2SaveId_ChestData, AM2FileId_ChestData),
    _i(AM2SaveId_MerchantData, AM2FileId_MerchantData)
#undef _i
};

char *AM2GetSaveIdPath(char path[AM2_PATH_MAX], enum AM2DiskMode mode,
                       enum AM2SaveId save, enum AM2SaveSlot slot)
{
  if (path != NULL) {
    if ((AM2SaveId_None < save) && (save < AM2_SAVE_ID_COUNT) &&
        ((AM2SaveSlot_None == slot) ||
         ((AM2_SAVE_SLOT_MIN <= slot) && (slot <= AM2_SAVE_SLOT_MAX)))) {
      char *const name = &path[AM2GetRootPath(path, mode, AM2DiskId_Save)];
      strcpy(name, "Save.00/");
      strcpy(name + sizeof("Save.00/") - sizeof(""),
             AM2FileIdInfo[AM2SaveFileId[save]].fileName);
      if (slot != AM2SaveSlot_None) {
        name[sizeof("Save.00/") - sizeof("") - 3] = "0123456789"[slot / 10];
        name[sizeof("Save.00/") - sizeof("") - 2] = "0123456789"[slot % 10];
      }
      return name;
    }
    path[0] = '\0';
  }
  return NULL;
}
