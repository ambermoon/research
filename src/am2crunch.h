#ifndef AM2CRUNCH_H
#define AM2CRUNCH_H

#include "am2def.h"

#ifdef __cplusplus
extern "C" {
#endif

enum {
  /*!
   * \brief  Size of the compression header magic value.
   *
   * For file streams, the compression detection is done after detecting the
   * optional encryption header with #AM2CipherHeaderRead() and decrypting the
   * remaining file data after #AM2_CIPHER_HEADER_SIZE with #AM2CryptFull().
   *
   * The item data in AMPC and AMNP archives starts with a magic value to
   * detect compressed item streams. If the magic value is not present, the
   * value is skipped and the raw (AMPC) or encrypted (AMNP) data follows.
   * If the item data is compressed and encrypted (AMNP), the item data has
   * to be decrypted, starting at offset 8 (header - magic size), before
   * reading the compression header with #AM2CrunchHeaderRead().
   */
  AM2_CRUNCH_MAGIC_SIZE = (size_t)4,
  /*!
   * \brief  Ambermoon compression header magic value (big-endian).
   *
   * Ambermoon (and this library) only supports a single
   * compressed data block with "LOB" as the magic value.
   */
  AM2_CRUNCH_MAGIC_VALUE = AM2UINT32_C(0x014C4F42),
  /*!
   * \brief  Maximum compression data sizes.
   *
   * The compressed data size in the header is limited to 24 bits.
   * Theoretically, the compressed data size could be larger, but
   * in the context of Ambermoon the limit is used for both sizes
   * (the data should be considered incompressible in this case).
   */
  AM2_CRUNCH_SIZE_MAX = AM2UINT32_C(0xFFFFFF),
  /*!
   * \brief  Size of the compression header, including magic value.
   *
   * \code{.c}
   * struct {
   *   uint8_t count;    // has to be 1 for Ambermoon
   *   uint8_t magic[3]; // has to be "LOB" for Ambermoon
   *   struct {
   *     uint8_t method;     // has to be 6 for Ambermoon
   *     uint8_t rawSize[3]; // big-endian size of uncompressed data
   *     // part of encrypted item data block in AMNP archives
   *     uint8_t cmpSize[4]; // big-endian size of compressed data
   *   }[count];
   * };
   * \endcode
   */
  AM2_CRUNCH_HEADER_SIZE = (size_t)(AM2_CRUNCH_MAGIC_SIZE + 8)
};

/*!
 * \brief  Detect Ambermoon compression header.
 * \param[in]  head  Pointer to the start of the item data block or the
 *                   start of the (already decrypted) file stream data.
 * \return           Returns \c true if the compression header is present.
 *
 * \sa  #AM2_CRUNCH_MAGIC_SIZE, #AM2_CRUNCH_MAGIC_VALUE
 */
bool AM2IsCrunchMagic(unsigned char const head[AM2_CRUNCH_MAGIC_SIZE]);

/*!
 * \brief  Validate compression method and retrieve the data sizes.
 * \param[in]   head     Pointer to the start of the already decrypted data
 *                       (AMPC/AMNP archive item data or file stream data).
 * \param[out]  rawSize  Optional pointer to the uncompressed data size.
 * \param[out]  cmpSize  Optional pointer to the compressed data size.
 * \return               Returns \c true if the
 *                       compression method is 6 (LZSS),
 *                       rawSize is greater than or equal to 1, and
 *                       cmpSize is greater than or equal to 2
 *                       and less than or equal to #AM2_CRUNCH_SIZE_MAX.
 * \note  The function does not test the magic value in the header.
 * \note  Make sure to decrypt AMNP item data before reading the header,
 *        else the compressed data size value is still encrypted.
 */
bool AM2CrunchHeaderRead(unsigned char const head[AM2_CRUNCH_HEADER_SIZE],
                         size_t *rawSize, size_t *cmpSize);

/*!
 * \brief  Fill a compression header.
 * \param[out]  head     Pointer to the compression header to write to.
 * \param[in]   rawSize  Optional pointer to the uncompressed data size.
 * \param[in]   cmpSize  Optional pointer to the compressed data size.
 * \return               Returns \c true if
 *                       rawSize is greater than or equal to 1,
 *                       cmpSize is greater than or equal to 2,
 *                       and both rawSize and cmpSize
 *                       are less than or equal to #AM2_CRUNCH_SIZE_MAX.
 * \note  The compression method written into the header is 6 (LZSS)
 *        and the magic is #AM2_CRUNCH_MAGIC_VALUE (1 block, "LOB").
 */
bool AM2CrunchHeaderWrite(unsigned char head[AM2_CRUNCH_HEADER_SIZE],
                          size_t rawSize, size_t cmpSize);

/*!
 * \brief  Flags for a compression operation result.
 */
enum AM2CrunchStatus {
  /*!
   * \brief  The data stream is valid and has no known issues.
   */
  AM2CrunchStatus_OK = 0,
  /*!
   * \brief  Flag that indicates invalid data or parameters.
   *
   * If no additional flags are present, this indicates an
   * invalid parameter (null pointer and/or invalid size).
   */
  AM2CrunchStatus_EInvalid = (1U << 0U),
  /*!
   * \brief  Data stream with one additional byte.
   *
   * Such data streams are perfectly valid, they just contain
   * one extra byte at the end (the original compressed data
   * blocks are word-aligned with a zero padding byte if odd).
   */
  AM2CrunchStatus_PadByte = (1U << 1U),
  /*!
   * \brief  Data stream with additional unused bytes at the end.
   *
   * Still a valid data stream, but might contain/hide additional data.
   */
  AM2CrunchStatus_MoreData = (1U << 2U),
  /*!
   * \brief  Data stream with compressed data references.
   *
   * It is not flagged as invalid because an advanced compressor may use
   * this feature (match-code offset is zero). Such data streams can only
   * be decompressed in-place. Therefore, it should be considered invalid
   * if the decoding routine does not support the in-place decompression.
   */
  AM2CrunchStatus_InplaceRef = (1U << 3U),
  /*!
   * \brief  Self-modifying data stream.
   *
   * It is not flagged as invalid because a very advanced compressor may
   * use this feature (destination overflows into source buffer). Such data
   * streams can only be decompressed with a specific additional in-place
   * decompression size. Therefore, it should be considered invalid if the
   * decoding routine does not support the in-place decompression or if the
   * additional in-place decompression size for the game file might change.
   */
  AM2CrunchStatus_InplaceMod = (1U << 4U),
  /*!
   * \brief  End of compressed data before uncompressed data size is reached.
   */
  AM2CrunchStatus_EndOfData = (1U << 5U) | (unsigned)AM2CrunchStatus_EInvalid,
  /*!
   * \brief  Decompression overflows uncompressed data size.
   */
  AM2CrunchStatus_EOverflow = (1U << 6U) | (unsigned)AM2CrunchStatus_EInvalid,
  /*!
   * \brief  Decompression match-code reference to undefined data.
   */
  AM2CrunchStatus_EUndefined = (1U << 7U) | (unsigned)AM2CrunchStatus_EInvalid,
  /*!
   * \brief  In-place decompression would cause a buffer overflow.
   */
  AM2CrunchStatus_ENoInplace = (1U << 8U) | (unsigned)AM2CrunchStatus_EInvalid,
  /*!
   * \brief  Not enough memory available for the crunch operation.
   */
  AM2CrunchStatus_ENoMemory = (1U << 9U) | (unsigned)AM2CrunchStatus_EInvalid,
  /*!
   * \brief  Not compressible.
   *
   * Mostly because the compressed header/data is larger than the raw data.
   */
  AM2CrunchStatus_ENoCrunch = (1U << 10U) | (unsigned)AM2CrunchStatus_EInvalid
};

/*!
 * \brief  Verify a compressed data stream.
 * \param[in]  cmpSize   Size of the compressed data.
 * \param[in]  cmpData   Pointer to the compressed data.
 * \param[in]  rawSize   Size of the uncompressed data.
 * \param[in]  cmpSpace  Additional buffer size for in-place decompression.
 * \return               Returns a combination of the the status flags.
 * \retval  #AM2CrunchStatus_OK          \copybrief  AM2CrunchStatus_OK
 * \retval  #AM2CrunchStatus_EInvalid    \copybrief  AM2CrunchStatus_EInvalid
 * \retval  #AM2CrunchStatus_PadByte     \copybrief  AM2CrunchStatus_PadByte
 * \retval  #AM2CrunchStatus_MoreData    \copybrief  AM2CrunchStatus_MoreData
 * \retval  #AM2CrunchStatus_InplaceRef  \copybrief  AM2CrunchStatus_InplaceRef
 * \retval  #AM2CrunchStatus_InplaceMod  \copybrief  AM2CrunchStatus_InplaceMod
 * \retval  #AM2CrunchStatus_EndOfData   \copybrief  AM2CrunchStatus_EndOfData
 * \retval  #AM2CrunchStatus_EOverflow   \copybrief  AM2CrunchStatus_EOverflow
 * \retval  #AM2CrunchStatus_EUndefined  \copybrief  AM2CrunchStatus_EUndefined
 * \retval  #AM2CrunchStatus_ENoInplace  \copybrief  AM2CrunchStatus_ENoInplace
 * \note  This function will immediately return #AM2CrunchStatus_InplaceMod
 *        when detecting a self-modifying data stream (no further checks are
 *        done, since the function does not simulate in-place decompression).
 * \sa  #AM2CrunchStatus
 */
enum AM2CrunchStatus AM2CrunchDataTest(size_t cmpSize,
                                       unsigned char const cmpData[/*cmpSize*/],
                                       size_t rawSize, am2u16_t cmpSpace);

/*!
 * \brief  Decompress a full data stream (not-in-place).
 * \param[in]   cmpSize   Size of the compressed data.
 * \param[in]   cmpData   Pointer to the compressed data.
 * \param[in]   rawSize   Size of the uncompressed data.
 * \param[out]  rawData   Pointer to the uncompressed data.
 * \return                Returns a combination of the the status flags.
 * \retval  #AM2CrunchStatus_OK          \copybrief  AM2CrunchStatus_OK
 * \retval  #AM2CrunchStatus_EInvalid    \copybrief  AM2CrunchStatus_EInvalid
 * \retval  #AM2CrunchStatus_PadByte     \copybrief  AM2CrunchStatus_PadByte
 * \retval  #AM2CrunchStatus_MoreData    \copybrief  AM2CrunchStatus_MoreData
 * \retval  #AM2CrunchStatus_InplaceRef  \copybrief  AM2CrunchStatus_InplaceRef
 * \retval  #AM2CrunchStatus_EndOfData   \copybrief  AM2CrunchStatus_EndOfData
 * \retval  #AM2CrunchStatus_EOverflow   \copybrief  AM2CrunchStatus_EOverflow
 * \retval  #AM2CrunchStatus_EUndefined  \copybrief  AM2CrunchStatus_EUndefined
 * \note  The behavior is undefined if the objects overlap.
 * \note  In-place decompression specific features are not supported by this
 *        function. In-place and/or undefined references are filled with zeros.
 *        The #AM2CrunchStatus_EInvalid flag is always included if the
 *        #AM2CrunchStatus_InplaceRef flag is present in the returned status.
 * \sa  #AM2CrunchStatus
 */
enum AM2CrunchStatus AM2DecompressRestrict(
    size_t cmpSize, unsigned char const cmpData[AM2RESTRICT /*cmpSize*/],
    size_t rawSize, unsigned char rawData[AM2RESTRICT /*rawSize*/]);

/*!
 * \brief  Compress a full data block (in-place).
 * \param[in]      size      Size of the input raw data.
 * \param[in,out]  data      Pointer to the raw data on input,
 *                           and compression header/data on output.
 * \param[in]      cmpSpace  Additional buffer size for in-place decompression.
 * \return                   Returns a combination of the the status flags.
 * \retval  #AM2CrunchStatus_OK          \copybrief  AM2CrunchStatus_OK
 * \retval  #AM2CrunchStatus_EInvalid    \copybrief  AM2CrunchStatus_EInvalid
 * \retval  #AM2CrunchStatus_PadByte     \copybrief  AM2CrunchStatus_PadByte
 * \retval  #AM2CrunchStatus_InplaceRef  \copybrief  AM2CrunchStatus_InplaceRef
 * \retval  #AM2CrunchStatus_ENoMemory   \copybrief  AM2CrunchStatus_ENoMemory
 * \note  TODO: cmpSpace is not yet checked.
 * \note  For compatibility reasons this function does not generate data
 *        streams with in-place specific features (#AM2CrunchStatus_InplaceMod
 *        always includes #AM2CrunchStatus_EInvalid), and data streams with an
 *        odd size will be padded with an additional zero byte.
 * \sa  #AM2CrunchStatus
 */
enum AM2CrunchStatus AM2CompressInPlace(size_t size,
                                        unsigned char data[/*size*/],
                                        am2u16_t cmpSpace);

#ifdef __cplusplus
}
#endif

#endif
