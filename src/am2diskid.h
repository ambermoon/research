#ifndef AM2DISKID_H
#define AM2DISKID_H

#include "am2def.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief  Ambermoon game file identifier.
 */
enum AM2DiskId {
  AM2DiskId_None, /*!< Invalid disk or hard disk mode. */
  AM2DiskId_A,    /*!< AMBER_A: install, play in floppy disk mode */
  AM2DiskId_B,    /*!< AMBER_B: intro */
  AM2DiskId_C,    /*!< AMBER_C: map-1 */
  AM2DiskId_D,    /*!< AMBER_D: map-2 */
  AM2DiskId_E,    /*!< AMBER_E: map-2 */
  AM2DiskId_F,    /*!< AMBER_F: map-3 */
  AM2DiskId_G,    /*!< AMBER_G: game */
  AM2DiskId_H,    /*!< AMBER_H: monster */
  AM2DiskId_I,    /*!< AMBER_I: music, extro */
  AM2DiskId_Save, /*!< AMBER_J: saved games (created with install disk) */
  AM2_DISK_ID_COUNT
};

enum {
  /*!
   * \brief  Length of the relative base path for hard disk game files.
   * \note  Does not include the terminating null character.
   */
  AM2_ROOT_LEN_DH = sizeof("Amberfiles/") - sizeof(""),
  /*!
   * \brief  Length of the absolute base path for floppy disk game files.
   * \note  Does not include the terminating null character.
   */
  AM2_ROOT_LEN_DF = sizeof("AMBER_?:") - sizeof(""),
  /*!
   * \brief  Maximum buffer size of the base path for game files.
   * \note  Includes the terminating null character.
   */
  AM2_ROOT_MAX = AM2_ROOT_LEN_DH + sizeof("")
};

/*!
 * \brief  Ambermoon disk mode.
 */
enum AM2DiskMode {
  /*!
   * \brief  Ambermoon installation on a single (hard) disk.
   */
  AM2DiskMode_HDInst,
  /*!
   * \brief  Playing Ambermoon from floppy disks.
   */
  AM2DiskMode_Floppy
};

/*!
 * \brief  Get the base path for game files by disk mode/id.
 * \param[out]  root  Pointer to the character array to write to.
 * \param[in]   mode  Ambermoon disk mode.  \sa  #AM2DiskMode
 * \param[in]   disk  Disk identifier for #AM2DiskMode_Floppy.
 * \return            Returns count of characters written,
 *                    not including the terminating null character.
 * \note  The returned string is an AmigaOS path, where the root directory
 *        (drive) separator is ":", and the directory separator is "/".
 */
size_t AM2GetRootPath(char root[AM2_ROOT_MAX], enum AM2DiskMode mode,
                      enum AM2DiskId disk);

#ifdef __cplusplus
}
#endif

#endif
