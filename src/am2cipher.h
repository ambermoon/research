#ifndef AM2CIPHER_H
#define AM2CIPHER_H

#include "am2def.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief  Data type for the cipher key/seed value.
 */
typedef am2u16_t AM2CipherKey;

enum {
  /*!
   * \brief  Cipher seed value that has been used by the game developers.
   *
   * Any random value would be fine, but the use of a fixed value
   * allows the creation of stable binaries for public releases.
   */
  AM2_CIPHER_KEY_DEFAULT = (AM2CipherKey)53991
};

enum {
  /*!
   * \brief  Ambermoon encryption header magic value (big-endian).
   */
  AM2_CIPHER_MAGIC_VALUE = AM2UINT16_C(0x4A48),
  /*!
   * \brief  Size of the optional encryption header for file streams.
   *
   * Use #AM2CipherHeaderRead() to detect the presence of the header
   * and #AM2CipherHeaderWrite() to initialize the encryption header.
   *
   * \code{.c}
   * struct {
   *   uint16_t magic;  // 0x4A48 ("JH")
   *   uint16_t seedx;  // seed = seedx ^ magic
   * };
   * \endcode
   */
  AM2_CIPHER_HEADER_SIZE = (size_t)4
};

/*!
 * \brief  Detect a file stream encryption header and retrieve the seed value.
 * \param[in]   head  Pointer to the start of the file stream data.
 * \param[out]  seed  Optional pointer to the decoded cipher seed value.
 * \return            Returns \c true if the header is present.
 */
bool AM2CipherHeaderRead(unsigned char const head[AM2_CIPHER_HEADER_SIZE],
                         AM2CipherKey *seed);

/*!
 * \brief  Fill a file stream encryption header.
 * \param[out]  head  Pointer to the start of the file stream data.
 * \param[in]   seed  Cipher seed value for the file stream.
 */
void AM2CipherHeaderWrite(unsigned char head[AM2_CIPHER_HEADER_SIZE],
                          AM2CipherKey seed);

/*!
 * \brief  Advance the cipher key for the next 16-bit data word.
 * \param[in]  key  Cipher key for the previous 16-bit data word.
 * \return          Cipher key for the following 16-bit data word.
 */
AM2CipherKey AM2CipherKeyNext(AM2CipherKey key);

/*!
 * \brief  Encrypt/decrypt a full data block.
 * \param[in]      seed  Encryption seed value for the data block.
 * \param[in]      size  Size of the data block in bytes.
 * \param[in,out]  data  Pointer to the start of the data block.
 */
void AM2CryptFull(AM2CipherKey seed, size_t size, unsigned char data[/*size*/]);

/*!
 * \brief  Data type for continuous encryption/decryption.
 *
 * To support continuous encryption/decryption in arbitrary steps with
 * #AM2CipherCryptNext(), the 17th bit is used to support odd steps.
 */
typedef am2u32_t AM2CipherKeyState;

/*!
 * \brief  Encrypt/decrypt next data bytes.
 * \param[in]      state  Previous cipher key state or seed value.
 * \param[in]      size   Number of data bytes to encrypt/decrypt.
 * \param[in,out]  data   Pointer to the current data to process.
 * \return                Cipher key state for the following data.
 */
AM2CipherKeyState AM2CryptNext(AM2CipherKeyState state, size_t size,
                               unsigned char data[/*size*/]);

#ifdef __cplusplus
}
#endif

#endif
