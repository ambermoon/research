#include "am2fileid.h"
#include <string.h>

struct AM2FileInfo const AM2FileIdInfo[AM2_FILE_ID_COUNT] = {
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
#define _i(ID, FT, FD1, FD2, FD3, FN, MP, MT, CX)                              \
  [ID] = {.fileType = FT,                                                      \
          .fileDisk = {FD1, FD2, FD3},                                         \
          .fileName = FN,                                                      \
          .reserved = {'\0', '\0'},                                            \
          .memPrio = MP,                                                       \
          .memType = MT,                                                       \
          .cmpSpace = CX}
#else
#define _i(ID, FT, FD1, FD2, FD3, FN, MP, MT, CX)                              \
  {                                                                            \
    FT, {FD1, FD2, FD3}, FN, {'\0', '\0'}, MP, MT, CX                          \
  }
#endif
    _i(AM2FileId_None, AM2FileType_None, AM2DiskId_None, AM2DiskId_None,
       AM2DiskId_None, "", 0, 3, 1024),
    _i(AM2FileId_PartyData, AM2FileType_File, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Party_data.sav", 100, 3, 0),
    _i(AM2FileId_PartyChar, AM2FileType_Item, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Party_char.amb", 50, 3, 0),
    _i(AM2FileId_NpcChar, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "NPC_char.amb", 100, 3, 1024),
    _i(AM2FileId_MonsterChar, AM2FileType_Item, AM2DiskId_H, AM2DiskId_None,
       AM2DiskId_None, "Monster_char_data.amb", 110, 3, 1024),
    _i(AM2FileId_Portrait, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Portraits.amb", 120, 1, 1024),
    _i(AM2FileId_MapData, AM2FileType_Split, AM2DiskId_C, AM2DiskId_D,
       AM2DiskId_F, "?Map_data.amb", 120, 3, 1024),
    _i(AM2FileId_IconData, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Icon_data.amb", 120, 3, 1024),
    _i(AM2FileId_IconGfx, AM2FileType_Split, AM2DiskId_C, AM2DiskId_D,
       AM2DiskId_F, "?Icon_gfx.amb", 120, 1, 1024),
    _i(AM2FileId_TravelGfx, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Travel_gfx.amb", 121, 1, 1024),
    _i(AM2FileId_NpcGfx, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "NPC_gfx.amb", 120, 1, 1024),
    _i(AM2FileId_LabData, AM2FileType_Split, AM2DiskId_None, AM2DiskId_D,
       AM2DiskId_F, "?Lab_data.amb", 110, 3, 1024),
    _i(AM2FileId_Wall3D_Chip, AM2FileType_Split, AM2DiskId_None, AM2DiskId_E,
       AM2DiskId_F, "?Wall3D.amb", 99, 1, 1024),
    _i(AM2FileId_LabBack, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Lab_background.amb", 90, 3, 1024),
    _i(AM2FileId_Pic80x80, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Pics_80x80.amb", 80, 1, 1024),
    _i(AM2FileId_MapText, AM2FileType_Split, AM2DiskId_C, AM2DiskId_D,
       AM2DiskId_F, "?Map_texts.amb", 50, 3, 2048),
    _i(AM2FileId_ChestData, AM2FileType_Item, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Chest_data.amb", 80, 3, 0),
    _i(AM2FileId_MonsterGroup, AM2FileType_Item, AM2DiskId_H, AM2DiskId_None,
       AM2DiskId_None, "Monster_groups.amb", 90, 3, 1024),
    _i(AM2FileId_MerchantData, AM2FileType_Item, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Merchant_data.amb", 90, 3, 0),
    _i(AM2FileId_CombatBack, AM2FileType_Item, AM2DiskId_H, AM2DiskId_None,
       AM2DiskId_None, "Combat_background.amb", 100, 1, 4096),
    _i(AM2FileId_MonsterGfx, AM2FileType_Item, AM2DiskId_H, AM2DiskId_None,
       AM2DiskId_None, "Monster_gfx.amb", 0, 1, 1024),
    _i(AM2FileId_Automap, AM2FileType_Item, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Automap.amb", 0, 3, 0),
    _i(AM2FileId_ObjectText, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Object_texts.amb", 0, 3, 2048),
    _i(AM2FileId_EventPic, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Event_pix.amb", 50, 1, 1024),
    _i(AM2FileId_Layout, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Layouts.amb", 127, 3, 1024),
    _i(AM2FileId_Music, AM2FileType_Item, AM2DiskId_I, AM2DiskId_None,
       AM2DiskId_None, "Music.amb", 99, 1, 4096),
    _i(AM2FileId_Palette, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Palettes.amb", 101, 3, 1024),
    _i(AM2FileId_RiddlemouthGfx, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Riddlemouth_graphics", 0, 1, 1024),
    _i(AM2FileId_Dictionary, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Dictionary.german", 0, 3, 1024),
    _i(AM2FileId_Object3D_Chip, AM2FileType_Split, AM2DiskId_None, AM2DiskId_D,
       AM2DiskId_F, "?Object3D.amb", 90, 1, 1024),
    _i(AM2FileId_Overlay3D, AM2FileType_Split, AM2DiskId_None, AM2DiskId_E,
       AM2DiskId_F, "?Overlay3D.amb", 80, 3, 1024),
    _i(AM2FileId_Wall3D_Fast, AM2FileType_Split, AM2DiskId_None, AM2DiskId_E,
       AM2DiskId_F, "?Wall3D.amb", 99, 2, 1024),
    _i(AM2FileId_ZoomBuffer, AM2FileType_None, AM2DiskId_None, AM2DiskId_None,
       AM2DiskId_None, "", 0, 3, 0),
    _i(AM2FileId_Object3D_Fast, AM2FileType_Split, AM2DiskId_None, AM2DiskId_D,
       AM2DiskId_F, "?Object3D.amb", 90, 2, 1024),
    _i(AM2FileId_Saves, AM2FileType_File, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Saves", 0, 3, 0),
    _i(AM2FileId_PartyText, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Party_texts.amb", 100, 3, 1024),
    _i(AM2FileId_NpcText, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "NPC_texts.amb", 70, 3, 2048),
    _i(AM2FileId_AutomapGfx, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Automap_graphics", 120, 1, 1024),
    _i(AM2FileId_PlaceData, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Place_data", 0, 3, 1024),
    _i(AM2FileId_CombatGfx, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Combat_graphics", 120, 1, 1024),
    _i(AM2FileId_FloorCeiling, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Floors.amb", 70, 3, 1024),
    _i(AM2FileId_BabyChar, AM2FileType_Item, AM2DiskId_Save, AM2DiskId_None,
       AM2DiskId_None, "Save.00/Party_char.amb", 50, 3, 0),
    _i(AM2FileId_StationaryGfx, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Stationary", 98, 1, 1024),
    _i(AM2FileId_PartyGfx, AM2FileType_Item, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Party_gfx.amb", 121, 1, 1024),
    _i(AM2FileId_ObjectGfx, AM2FileType_File, AM2DiskId_G, AM2DiskId_None,
       AM2DiskId_None, "Object_icons", 127, 3, 1024)
#undef _i
};

char *AM2GetFileIdPath(char path[AM2_PATH_MAX], enum AM2DiskMode mode,
                       enum AM2FileId file, AM2FileItemId mapData)
{
  if (path != NULL) {
    if ((AM2FileId_None < file) && (file < AM2_FILE_ID_COUNT)) {
      struct AM2FileInfo const *const info = &AM2FileIdInfo[file];
      size_t disk = 0U;
      if ((AM2FileType_Split == info->fileType) && (256 < mapData)) {
        ++disk;
        if ((300 <= mapData) && (mapData <= 399)) {
          ++disk;
        }
      }
      if (info->fileDisk[disk] != AM2DiskId_None) {
        char *const name =
            &path[AM2GetRootPath(path, mode, info->fileDisk[disk])];
        strcpy(name, info->fileName);
        if (AM2FileType_Split == info->fileType) {
          *name = "123"[disk];
        }
        return name;
      }
    }
    path[0] = '\0';
  }
  return NULL;
}
