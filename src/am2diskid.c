#include "am2diskid.h"
#include <string.h>

size_t AM2GetRootPath(char root[AM2_ROOT_MAX], enum AM2DiskMode mode,
                      enum AM2DiskId disk)
{
  if (root != NULL) {
    switch (mode) {
    case AM2DiskMode_HDInst: {
      AM2_STATIC_ASSERT(AM2_ROOT_LEN_DH == sizeof("Amberfiles/") - sizeof(""),
                        AM2_ROOT_LEN_DH__mismatch);
      strcpy(root, "Amberfiles/");
      return AM2_ROOT_LEN_DH;
    }
    case AM2DiskMode_Floppy:
      if ((AM2DiskId_A <= disk) && (disk < AM2_DISK_ID_COUNT)) {
        AM2_STATIC_ASSERT(AM2_ROOT_LEN_DF == sizeof("AMBER_?:") - sizeof(""),
                          AM2_ROOT_LEN_DF__mismatch);
        AM2_STATIC_ASSERT(AM2_DISK_ID_COUNT - AM2DiskId_A ==
                              sizeof("ABCDEFGHIJ") - sizeof(""),
                          AM2_DISK_ID_COUNT__mismatch);
        strcpy(root, "AMBER_?:");
        root[AM2_ROOT_LEN_DF - 2] = "ABCDEFGHIJ"[disk - AM2DiskId_A];
        return AM2_ROOT_LEN_DF;
      }
      break;
    default:
      break;
    }
    *root = '\0';
  }
  return 0U;
}
