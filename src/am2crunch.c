#include "am2crunch.h"
#include <stdlib.h>

bool AM2IsCrunchMagic(unsigned char const head[AM2_CRUNCH_MAGIC_SIZE])
{
  return ((AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 24U) == head[0]) &&
          (AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 16U) == head[1]) &&
          (AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 8U) == head[2]) &&
          (AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 0U) == head[3]))
             ? true
             : false;
}

bool AM2CrunchHeaderRead(unsigned char const head[AM2_CRUNCH_HEADER_SIZE],
                         size_t *rawSize, size_t *cmpSize)
{
  AM2_STATIC_ASSERT((sizeof(size_t) * CHAR_BIT) >= 32,
                    CrunchHeaderRead__size_overflow);
  am2u32_t const raw = ((am2u32_t)((am2u32_t)head[5] << 16U) |
                        (am2u32_t)((am2u32_t)head[6] << 8U) |
                        (am2u32_t)((am2u32_t)head[7] << 0U));
  am2u32_t const cmp = ((am2u32_t)((am2u32_t)head[8] << 24U) |
                        (am2u32_t)((am2u32_t)head[9] << 16U) |
                        (am2u32_t)((am2u32_t)head[10] << 8U) |
                        (am2u32_t)((am2u32_t)head[11] << 0U));
  if (rawSize != NULL) {
    *rawSize = raw;
  }
  if (cmpSize != NULL) {
    *cmpSize = cmp;
  }
  return ((AM2UINT8_C(6) == head[4]) && (AM2UINT32_C(1) <= raw) &&
          (AM2UINT32_C(2) <= cmp) && (cmp <= (am2u32_t)AM2_CRUNCH_SIZE_MAX))
             ? true
             : false;
}

bool AM2CrunchHeaderWrite(unsigned char head[AM2_CRUNCH_HEADER_SIZE],
                          size_t rawSize, size_t cmpSize)
{
  AM2_STATIC_ASSERT((sizeof(size_t) * CHAR_BIT) > 24,
                    CrunchHeaderWrite__size_shift_overflow);
  /* magic value */
  head[0] = AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 24U);
  head[1] = AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 16U);
  head[2] = AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 8U);
  head[3] = AM2UINT8_CAST((am2u32_t)AM2_CRUNCH_MAGIC_VALUE >> 0U);
  /* compression method (LZSS) */
  head[4] = AM2UINT8_C(6);
  /* uncompressed data size */
  head[5] = AM2UINT8_CAST(rawSize >> 16U);
  head[6] = AM2UINT8_CAST(rawSize >> 8U);
  head[7] = AM2UINT8_CAST(rawSize >> 0U);
  /* compressed data size */
  head[8] = AM2UINT8_CAST(cmpSize >> 24U);
  head[9] = AM2UINT8_CAST(cmpSize >> 16U);
  head[10] = AM2UINT8_CAST(cmpSize >> 8U);
  head[11] = AM2UINT8_CAST(cmpSize >> 0U);
  return (((size_t)1 <= rawSize) && (rawSize <= (size_t)AM2_CRUNCH_SIZE_MAX) &&
          ((size_t)2 <= cmpSize) && (cmpSize <= (size_t)AM2_CRUNCH_SIZE_MAX))
             ? true
             : false;
}

enum AM2CrunchStatus AM2CrunchDataTest(size_t cmpSize,
                                       unsigned char const cmpData[/*cmpSize*/],
                                       size_t rawSize, am2u16_t cmpSpace)
{
  unsigned status;
  bool cmpOdd;
  size_t rawPos, cmpPos;
  size_t undefBegin, undefEnd;
  if ((cmpSize < (size_t)2) || ((size_t)AM2_CRUNCH_SIZE_MAX < cmpSize) ||
      (rawSize < (size_t)1) || ((size_t)AM2_CRUNCH_SIZE_MAX < rawSize) ||
      (NULL == cmpData)) {
    return AM2CrunchStatus_EInvalid;
  }
  /* The original in-place decompression routine also receives the header and
   * moves cmpSize + 8 (header - magic) data bytes (in two long-word steps)
   * backwards from head[4 + cmpSize + 8] to head[rawSize + cmpSpace]. This
   * results in a buffer overflow if rawSize and/or cmpSpace are too small. */
  {
    /* the post-test loop uses `SUBQ.L #8,N` `BPL.S @@MOVE8`. Therefore,
     * it loops once more if the compressed data size is a multiple of 8 */
    size_t const cmpMove = cmpSize + (size_t)(8 + 8) - (size_t)(cmpSize % 8U);
    if ((rawSize + cmpSpace) < cmpMove) {
      return AM2CrunchStatus_ENoInplace;
    }
    undefBegin = cmpSize;
    /* we have to take care about overflows here, because the move may or
     * may not include the magic and/or undefined data before the header */
    undefEnd = (rawSize + cmpSpace) -
               ((cmpMove > (cmpSize + (size_t)AM2_CRUNCH_HEADER_SIZE))
                    ? (size_t)(cmpSize + (size_t)AM2_CRUNCH_HEADER_SIZE)
                    : cmpMove);
  }
  status = AM2CrunchStatus_OK;
  cmpOdd = (cmpSize & 1U) ? true : false;
  rawPos = (size_t)0;
  cmpPos = (rawSize + cmpSpace) - cmpSize;
  while (cmpSize > (size_t)0) {
    uint_fast8_t flag;
    uint_fast8_t const flags = *cmpData;
    (--cmpSize, ++cmpData, ++cmpPos);
    for (flag = UINT8_C(0x80); flag != UINT8_C(0); flag >>= 1U) {
      if (flag & flags) {
        /* literal code */
        if (cmpSize < (size_t)1) {
          /* end of input */
          break;
        }
        (--cmpSize, ++cmpData, ++cmpPos);
        ++rawPos;
      } else {
        /* match code */
        if (cmpSize < (size_t)2) {
          /* end of input */
          cmpSize = (size_t)0;
          break;
        } else {
          size_t const len = (cmpData[0] & AM2UINT8_C(0x0F)) + 3U;
          size_t const off =
              ((size_t)(cmpData[0] & AM2UINT8_C(0xF0)) << 4U) | cmpData[1];
          (cmpSize -= (size_t)2, cmpData += (size_t)2, cmpPos += (size_t)2);
          if (off <= (size_t)0) {
            status |= (unsigned)AM2CrunchStatus_InplaceRef;
            /* if the compressed data size is less than start
             * of the initialized in-place buffer at the end,
             * the memory content is this range is undefined */
            if ((undefBegin <= rawPos) && (rawPos < undefEnd)) {
              status |= (unsigned)AM2CrunchStatus_EUndefined;
            }
          } else if (rawPos < off) {
            /* points before the start of the uncompressed data */
            status |= (unsigned)AM2CrunchStatus_EUndefined;
          }
          if (len > (rawSize - rawPos)) {
            status |= (unsigned)AM2CrunchStatus_EOverflow;
            rawPos = rawSize;
          } else {
            rawPos += len;
          }
        }
      }
      /* test for end of uncompressed data */
      if (rawPos >= rawSize) {
        if (cmpSize > 0U) {
          status |= (cmpOdd && (cmpSize == 1U))
                        ? (unsigned)AM2CrunchStatus_PadByte
                        : (unsigned)AM2CrunchStatus_MoreData;
        }
        return (enum AM2CrunchStatus)status;
      }
      /* test for self-modifying data stream */
      if (rawPos > cmpPos) {
        status |= (unsigned)AM2CrunchStatus_InplaceMod;
        /* have to stop here because the uncompressed data
         * is not available for further decompression... */
        return (enum AM2CrunchStatus)status;
      }
    }
  }
  status |= (unsigned)AM2CrunchStatus_EndOfData;
  return (enum AM2CrunchStatus)status;
}

enum AM2CrunchStatus AM2DecompressRestrict(
    size_t cmpSize, unsigned char const cmpData[AM2RESTRICT /*cmpSize*/],
    size_t rawSize, unsigned char rawData[AM2RESTRICT /*rawSize*/])
{
  unsigned status;
  size_t cmpPos;
  size_t rawPos;
  if ((cmpSize < (size_t)2) || (NULL == cmpData) || (rawSize < (size_t)1) ||
      (NULL == rawData)) {
    return AM2CrunchStatus_EInvalid;
  }
  status = AM2CrunchStatus_OK;
  cmpPos = (size_t)0;
  rawPos = (size_t)0;
  do {
    uint_fast8_t flag;
    uint_fast8_t const flags = cmpData[cmpPos++];
    for (flag = UINT8_C(0x80); (flag != UINT8_C(0)) && (cmpPos < cmpSize);
         flag >>= 1U) {
      unsigned char const cmpByte = cmpData[cmpPos++];
      if (flag & flags) {
        /* literal */
        rawData[rawPos++] = cmpByte;
      } else if (cmpPos >= cmpSize) {
        /* end of data */
        break;
      } else {
        /* match */
        size_t len = (cmpByte & AM2UINT8_C(0x0F)) + 3U;
        size_t const off =
            ((size_t)(cmpByte & AM2UINT8_C(0xF0)) << 4U) | cmpData[cmpPos++];
        if (len > (rawSize - rawPos)) {
          len = rawSize - rawPos;
          status |= (unsigned)AM2CrunchStatus_EOverflow;
        }
        do {
          unsigned char rawByte;
          if (off <= (size_t)0) {
            /* in-place decompression reference */
            status |= (unsigned)AM2CrunchStatus_EInvalid |
                      (unsigned)AM2CrunchStatus_InplaceRef;
            rawByte = AM2UINT8_C(0);
          } else if (rawPos < off) {
            /* points before the start of the uncompressed data */
            status |= (unsigned)AM2CrunchStatus_EUndefined;
            rawByte = AM2UINT8_C(0);
          } else {
            rawByte = rawData[rawPos - off];
          }
          rawData[rawPos++] = rawByte;
        } while (--len > (size_t)0);
      }
      /* test for end of uncompressed data */
      if (rawPos >= rawSize) {
        if (cmpPos < cmpSize) {
          status |= ((cmpPos & 1U) && ((cmpSize - cmpPos) == 1U))
                        ? (unsigned)AM2CrunchStatus_PadByte
                        : (unsigned)AM2CrunchStatus_MoreData;
        }
        return (enum AM2CrunchStatus)status;
      }
    }
  } while (cmpPos < cmpSize);
  status |= (unsigned)AM2CrunchStatus_EndOfData;
  return (enum AM2CrunchStatus)status;
}

enum AM2CrunchStatus
AM2CompressInPlace(size_t size, unsigned char data[/*size*/], am2u16_t cmpSpace)
{
  (void)cmpSpace; /*TODO: check for in-place decompression compatibility */
  if ((size < 1U) || (NULL == data)) {
    return AM2CrunchStatus_EInvalid;
  } else if (AM2_CRUNCH_SIZE_MAX < size) {
    /* raw data size exceeds header limit */
    return AM2CrunchStatus_ENoCrunch;
  } else {
    /* weighted directed acyclic graph */
    struct Vertex {
      union {
        size_t weight;
        size_t next;
      } u1;
      size_t prev;
      size_t length;
      union {
        size_t offset;
        size_t literal;
      } u2;
    } *graph = malloc((size + 1U) * sizeof(struct Vertex));
    if (NULL == graph) {
      return AM2CrunchStatus_ENoMemory;
    } else {
      enum AM2CrunchStatus result;
      size_t i;
      size_t cmpSize = 0U;
      /* first vertex is a literal */
      graph[1U].u1.weight = 9U;
      graph[1U].prev = 0U;
      graph[1U].length = 0U;
      /* initialize all other vertices */
      for (i = 2U; i < size + 1U; ++i) {
        graph[i].u1.weight = SIZE_MAX;
      }
      /* build graph with topological sorting */
      for (i = 1U; i < size; ++i) {
        size_t j;
        size_t const b = (i < 0xFFFU) ? 0U : (i - 0xFFFU);
        size_t const e = ((size - i) < 18U) ? (size - i) : 18U;
        for (j = i - 1U; b < j; --j) {
          size_t k;
          for (k = 0U; (k < e) && (data[i + k] == data[j + k]); ++k) {
            if ((k + 1U) >= 3U) {
              if (graph[i + k + 1U].u1.weight > (graph[i].u1.weight + 17U)) {
                graph[i + k + 1U].u1.weight = graph[i].u1.weight + 17U;
                graph[i + k + 1U].prev = i;
                graph[i + k + 1U].length = k + 1U;
                graph[i + k + 1U].u2.offset = j;
              }
            }
          }
        }
        if (graph[i + 1U].u1.weight >= (graph[i].u1.weight + 9U)) {
          graph[i + 1U].u1.weight = graph[i].u1.weight + 9U;
          graph[i + 1U].prev = i;
          graph[i + 1U].length = 0U;
        }
      }
      /* reverse graph for easier iteration */
      graph[0U].prev = SIZE_MAX;
      graph[size].u1.next = SIZE_MAX;
      for (i = size; graph[i].prev != SIZE_MAX; i = graph[i].prev) {
        graph[graph[i].prev].u1.next = i;
      }
      /* check compressed header/data size */
      for (i = 0U; graph[i].u1.next != SIZE_MAX; i = graph[i].u1.next) {
        if (0U == graph[graph[i].u1.next].length) {
          cmpSize += 9U;
          graph[graph[i].u1.next].u2.literal = data[i];
        } else {
          cmpSize += 17U;
        }
      }
      cmpSize += 7U;
      cmpSize /= 8U;
      if (cmpSize & 1U) {
        cmpSize += 1U;
      }
      cmpSize += AM2_CRUNCH_HEADER_SIZE;
      if (size < cmpSize) {
        result = AM2CrunchStatus_ENoCrunch;
      } else {
        /* write compressed data */
        unsigned char codes[8U * 2U];
        unsigned char *code = codes;
        unsigned char mask = 0x80U;
        unsigned char flags = 0U;
        unsigned char *dpos = data + AM2_CRUNCH_HEADER_SIZE;
        for (i = 0U; graph[i].u1.next != SIZE_MAX; i = graph[i].u1.next) {
          size_t const next = graph[i].u1.next;
          size_t const mlen = graph[next].length;
          if (0U == mlen) {
            flags |= mask;
            *code++ = AM2UINT8_CAST(graph[next].u2.literal);
          } else {
            size_t const dist = next - mlen - graph[next].u2.offset;
            *code++ = AM2UINT8_CAST(((dist & 0xF00U) >> 4U) | (mlen - 3U));
            *code++ = AM2UINT8_CAST(dist);
          }
          mask >>= 1U;
          if (0U == mask) {
            ptrdiff_t j;
            *dpos++ = flags;
            for (j = 0U; j < code - codes; ++j) {
              *dpos++ = codes[j];
            }
            code = codes;
            mask = 0x80U;
            flags = 0U;
          }
        }
        /* flush remaining flags/codes */
        if (code != codes) {
          ptrdiff_t j;
          *dpos++ = flags;
          for (j = 0U; j < code - codes; ++j) {
            *dpos++ = codes[j];
          }
        }
        /* done, pad and write header */
        result = AM2CrunchStatus_OK;
        cmpSize = dpos - data - AM2_CRUNCH_HEADER_SIZE;
        if (cmpSize & 1U) {
          *dpos = AM2UINT8_C(0);
          ++cmpSize;
          result |= (unsigned)AM2CrunchStatus_PadByte;
        }
        AM2CrunchHeaderWrite(data, size, cmpSize);
      }
      free(graph);
      return result;
    }
  }
}
