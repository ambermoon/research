#include "am2cipher.h"

bool AM2CipherHeaderRead(unsigned char const head[AM2_CIPHER_HEADER_SIZE],
                         AM2CipherKey *seed)
{
  if ((AM2UINT16_HIBYTE(AM2_CIPHER_MAGIC_VALUE) == head[0]) &&
      (AM2UINT16_LOBYTE(AM2_CIPHER_MAGIC_VALUE) == head[1])) {
    if (seed != NULL) {
      *seed = (AM2CipherKey)((AM2CipherKey)((AM2CipherKey)head[2] << 8U) |
                             (AM2CipherKey)head[3]) ^
              (AM2CipherKey)AM2_CIPHER_MAGIC_VALUE;
    }
    return true;
  }
  return false;
}

void AM2CipherHeaderWrite(unsigned char head[AM2_CIPHER_HEADER_SIZE],
                          AM2CipherKey seed)
{
  head[0] = AM2UINT16_HIBYTE(AM2_CIPHER_MAGIC_VALUE);
  head[1] = AM2UINT16_LOBYTE(AM2_CIPHER_MAGIC_VALUE);
  head[2] = AM2UINT16_HIBYTE(seed) ^ AM2UINT16_HIBYTE(AM2_CIPHER_MAGIC_VALUE);
  head[3] = AM2UINT16_LOBYTE(seed) ^ AM2UINT16_LOBYTE(AM2_CIPHER_MAGIC_VALUE);
}

AM2CipherKey AM2CipherKeyNext(AM2CipherKey key)
{
  return AM2UINT16_CAST(AM2UINT16_CAST(key * 17U) + 87U);
}

void AM2CryptFull(AM2CipherKey seed, size_t size, unsigned char data[/*size*/])
{
  AM2CipherKey key = seed;
  {
    size_t i;
    for (i = 0U; i < size / 2U; ++i) {
      *data++ ^= AM2UINT16_HIBYTE(key);
      *data++ ^= AM2UINT16_LOBYTE(key);
      key = AM2CipherKeyNext(key);
    }
  }
  if (size & 1U) {
    *data ^= AM2UINT16_HIBYTE(key);
  }
}

AM2CipherKeyState AM2CryptNext(AM2CipherKeyState state, size_t size,
                               unsigned char data[/*size*/])
{
  if (size > 0U) {
    /* handle odd offset from last iteration */
    if (AM2UINT32_C(0x10000) & state) {
      *data++ ^= AM2UINT16_LOBYTE(AM2UINT32_LOWORD(state));
      --size;
      state = AM2CipherKeyNext(AM2UINT32_LOWORD(state));
    }
    /* process full 16-bit words */
    {
      size_t i;
      for (i = 0U; i < size / 2U; ++i) {
        *data++ ^= AM2UINT16_HIBYTE(AM2UINT32_LOWORD(state));
        *data++ ^= AM2UINT16_LOBYTE(AM2UINT32_LOWORD(state));
        state = AM2CipherKeyNext(AM2UINT32_LOWORD(state));
      }
    }
    /* process remaining odd byte */
    if (size & 1U) {
      *data ^= AM2UINT16_HIBYTE(AM2UINT32_LOWORD(state));
      state |= AM2UINT32_C(0x10000);
    }
  }
  return state;
}
