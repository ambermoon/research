#ifndef AM2FILEID_H
#define AM2FILEID_H

#include "am2def.h"
#include "am2diskid.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief  Ambermoon game file identifier.
 *
 * Ambermoon opens all resource files by identifier
 * and every game resource has hard-coded properties.
 *
 * \sa  #AM2FileIdInfo
 */
enum AM2FileId {
  AM2FileId_None,           /*!< (run-time, memory block) */
  AM2FileId_PartyData,      /*!< AMBER_J:Party_data.sav */
  AM2FileId_PartyChar,      /*!< AMBER_J:Party_char.amb */
  AM2FileId_NpcChar,        /*!< AMBER_G:NPC_char.amb */
  AM2FileId_MonsterChar,    /*!< AMBER_H:Monster_char_data.amb */
  AM2FileId_Portrait,       /*!< AMBER_G:Portraits.amb */
  AM2FileId_MapData,        /*!< AMBER_[CDF]:[123]Map_data.amb */
  AM2FileId_IconData,       /*!< AMBER_G:Icon_data.amb */
  AM2FileId_IconGfx,        /*!< AMBER_[CDF]:[123]Icon_gfx.amb */
  AM2FileId_TravelGfx,      /*!< AMBER_G:Travel_gfx.amb */
  AM2FileId_NpcGfx,         /*!< AMBER_G:NPC_gfx.amb */
  AM2FileId_LabData,        /*!< AMBER_[DF]:[23]Lab_data.amb */
  AM2FileId_Wall3D_Chip,    /*!< AMBER_[EF]:[23]Wall3D.amb */
  AM2FileId_LabBack,        /*!< AMBER_G:Lab_background.amb */
  AM2FileId_Pic80x80,       /*!< AMBER_G:Pics_80x80.amb */
  AM2FileId_MapText,        /*!< AMBER_[CDF]:[123]Map_texts.amb */
  AM2FileId_ChestData,      /*!< AMBER_J:Chest_data.amb */
  AM2FileId_MonsterGroup,   /*!< AMBER_H:Monster_groups.amb */
  AM2FileId_MerchantData,   /*!< AMBER_J:Merchant_data.amb */
  AM2FileId_CombatBack,     /*!< AMBER_H:Combat_background.amb */
  AM2FileId_MonsterGfx,     /*!< AMBER_H:Monster_gfx.amb */
  AM2FileId_Automap,        /*!< AMBER_J:Automap.amb */
  AM2FileId_ObjectText,     /*!< AMBER_G:Object_texts.amb */
  AM2FileId_EventPic,       /*!< AMBER_G:Event_pix.amb */
  AM2FileId_Layout,         /*!< AMBER_G:Layouts.amb */
  AM2FileId_Music,          /*!< AMBER_I:Music.amb */
  AM2FileId_Palette,        /*!< AMBER_G:Palettes.amb */
  AM2FileId_RiddlemouthGfx, /*!< AMBER_G:Riddlemouth_graphics */
  AM2FileId_Dictionary,     /*!< AMBER_G:Dictionary.german */
  AM2FileId_Object3D_Chip,  /*!< AMBER_[DF]:[23]Object3D.amb */
  AM2FileId_Overlay3D,      /*!< AMBER_[EF]:[23]Overlay3D.amb */
  AM2FileId_Wall3D_Fast,    /*!< AMBER_[EF]:[23]Wall3D.amb */
  AM2FileId_ZoomBuffer,     /*!< (run-time, zoom buffer) */
  AM2FileId_Object3D_Fast,  /*!< AMBER_[DF]:[23]Object3D.amb */
  AM2FileId_Saves,          /*!< AMBER_J:Saves */
  AM2FileId_PartyText,      /*!< AMBER_G:Party_texts.amb */
  AM2FileId_NpcText,        /*!< AMBER_G:NPC_texts.amb */
  AM2FileId_AutomapGfx,     /*!< AMBER_G:Automap_graphics */
  AM2FileId_PlaceData,      /*!< AMBER_G:Place_data */
  AM2FileId_CombatGfx,      /*!< AMBER_G:Combat_graphics */
  AM2FileId_FloorCeiling,   /*!< AMBER_G:Floors.amb */
  AM2FileId_BabyChar,       /*!< AMBER_J:Save.00/Party_char.amb */
  AM2FileId_StationaryGfx,  /*!< AMBER_G:Stationary */
  AM2FileId_PartyGfx,       /*!< AMBER_G:Party_gfx.amb */
  AM2FileId_ObjectGfx,      /*!< AMBER_G:Object_icons */
  AM2_FILE_ID_COUNT
};

/*!
 * \brief  Base type for archive item identifiers.
 */
typedef am2u16_t AM2FileItemId;

enum {
  /*!
   * \brief  Invalid/no archive item identifier.
   */
  AM2FileItemId_None = (AM2FileItemId)0,
  /*!
   * \brief  Archive item identifiers re 1-based.
   */
  AM2_FILE_ITEM_ID_MIN = (AM2FileItemId)1,
  /*!
   * \brief  Maximum archive item count in the original game.
   */
  AM2_FILE_ITEM_ID_MAX = (AM2FileItemId)530
};

enum {
  /*!
   * \brief  Maximum item reference list length in the original game.
   */
  AM2_FILE_ITEM_ID_LIST_MAX = (size_t)64
};

/*!
 * \brief  File object type.
 */
enum AM2FileType {
  AM2FileType_None, /*!< Invalid/no file object (e.g. memory block). */
  AM2FileType_File, /*!< Single file, optionally encrypted/compressed. */
  AM2FileType_Item, /*!< Single archive file (AMBR/AMPC/AMNC/AMNP) */
  AM2FileType_Split /*!< Multi-part or cloned  archive (map data) */
};

enum {
  /*!
   * \brief  Maximum length of relative game file names.
   * \note  Does not include the terminating null character.
   */
  AM2_NAME_MAX = (size_t)32,
  /*!
   * \brief  Maximum buffer size of a full filename for game files.
   * \note  Includes the terminating null character.
   */
  AM2_PATH_MAX = (size_t)(AM2_ROOT_MAX + AM2_NAME_MAX)
};

/*!
 * \brief  Game file properties.
 */
struct AM2FileInfo {
  enum AM2FileType fileType;   /*!< File type.  \sa  #AM2FileType */
  enum AM2DiskId fileDisk[3];  /*!< Disk (0 File/Item, 0-2 Split). */
  char fileName[AM2_NAME_MAX]; /*!< Relative file path ('?'... for Split). */
  char reserved[2];            /*!< Null-termination and alignment. */
  am2u8_t memPrio;             /*!< Memory priority (MSB is reserved). */
  am2u8_t memType;             /*!< Memory type (1 Chip, 2 Fast, 3 = Any). */
  am2u16_t cmpSpace; /*!< Additional buffer size for in-place decompression. */
};

/*!
 * \brief  Table with all game file properties.
 *
 * This information has been extracted from several tables in the German
 * AM2_CPU v1.05 (might/will be different in other version and/or AM2_BLIT).
 */
extern struct AM2FileInfo const AM2FileIdInfo[AM2_FILE_ID_COUNT];

/*!
 * \brief  Get the full file path for a game file by id.
 * \param[out]  path     Pointer to the character array to write to.
 * \param[in]   mode     Ambermoon disk mode.  \sa  #AM2DiskMode
 * \param[in]   file     File identifier.  \sa  AM2FileId.
 * \param[in]   mapData  Map data item identifier for multi-part archives.
 * \return               Returns a pointer to the start of the relative file
 *                       name (in path), or \c NULL for invalid parameters.
 * \note  The returned string is an AmigaOS path, where the root directory
 *        (drive) separator is ':', and the directory separator is "/".
 */
char *AM2GetFileIdPath(char path[AM2_PATH_MAX], enum AM2DiskMode mode,
                       enum AM2FileId file, AM2FileItemId mapData);

#ifdef __cplusplus
}
#endif

#endif
