#ifndef AM2DEF_H
#define AM2DEF_H

#ifdef __cplusplus
#include <cstdbool>
#include <cstddef>
#include <cstdint>
#include <climits>
#else
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <limits.h>
#endif

#ifndef __cplusplus
#if !defined(__bool_true_false_are_defined) || (!__bool_true_false_are_defined)
#ifdef bool
#undef bool
#endif
#ifdef true
#undef true
#endif
#ifdef false
#undef false
#endif
enum bool { false, true };
#endif
#endif

typedef int_least8_t am2i8_t;
typedef uint_least8_t am2u8_t;
typedef int_least16_t am2i16_t;
typedef uint_least16_t am2u16_t;
typedef int_least32_t am2i32_t;
typedef uint_least32_t am2u32_t;

#ifdef __cplusplus
#define AM2INT8_C(x) static_cast<am2i8_t>(INT8_C(x))
#define AM2UINT8_C(x) static_cast<am2u8_t>(UINT8_C(x))
#define AM2INT16_C(x) static_cast<am2i16_t>(INT16_C(x))
#define AM2UINT16_C(x) static_cast<am2u16_t>(UINT16_C(x))
#define AM2INT32_C(x) static_cast<am2i32_t>(INT32_C(x))
#define AM2UINT32_C(x) static_cast<am2u32_t>(UINT32_C(x))
#else
#define AM2INT8_C(x) (am2i8_t)(INT8_C(x))
#define AM2UINT8_C(x) (am2u8_t)(UINT8_C(x))
#define AM2INT16_C(x) (am2i16_t)(INT16_C(x))
#define AM2UINT16_C(x) (am2u16_t)(UINT16_C(x))
#define AM2INT32_C(x) (am2i32_t)(INT32_C(x))
#define AM2UINT32_C(x) (am2u32_t)(UINT32_C(x))
#endif

#ifdef __cplusplus
#define AM2UINT8_CAST(x) static_cast<am2u8_t>(static_cast<am2u8_t>(x) & AM2UINT8_C(0xFF))
#define AM2UINT16_CAST(x) static_cast<am2u16_t>(static_cast<am2u16_t>(x) & AM2UINT16_C(0xFFFF))
#define AM2UINT32_CAST(x) static_cast<am2u32_t>(static_cast<am2u32_t>(x) & AM2UINT32_C(0xFFFFFFFF))
#else
#define AM2UINT8_CAST(x) (am2u8_t)((am2u8_t)(x) & AM2UINT8_C(0xFF))
#define AM2UINT16_CAST(x) (am2u16_t)((am2u16_t)(x) & AM2UINT16_C(0xFFFF))
#define AM2UINT32_CAST(x) (am2u32_t)((am2u32_t)(x) & AM2UINT32_C(0xFFFFFFFF))
#endif
#define AM2UINT16_LOBYTE(x) AM2UINT8_CAST(AM2UINT16_CAST(x))
#define AM2UINT16_HIBYTE(x) AM2UINT8_CAST(AM2UINT16_CAST(x) >> 8U)
#define AM2UINT32_LOWORD(x) AM2UINT16_CAST(AM2UINT32_CAST(x))
#define AM2UINT32_HIWORD(x) AM2UINT16_CAST(AM2UINT32_CAST(x) >> 16U)

#if !defined(__cplusplus) && defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
#define AM2RESTRICT restrict
#else
#define AM2RESTRICT /*restrict*/
#endif

#define AM2_STATIC_ASSERT(cond, name) enum { AM2_ASSERT__##name = 1 / (cond) }

#endif
