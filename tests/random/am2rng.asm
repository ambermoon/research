;
; Reference implementation of AM2Random.
;
; Usage: am2rng_r > am2rng_r.txt
; Must be run from CLI and requires AmigaDOS 2.0+ (V36)
;
; Build: vasmm68k_mot[_<HOST>] -Fhunkexe -o am2rng_r am2rng.asm
;
		bra.b	am2rng_r
	align	2
AM2Random:
		move.l	d1,-(sp)
		movem.w	(.seed).l,d0/d1
		ror.w	#1,d0
		addq.w	#7,d0
		eor.w	d1,d0
		exg	d0,d1
		movem.w	d0/d1,(.seed).l
		andi.l	#$FFFF,d0
		move.l	(sp)+,d1
		rts
.seed:
		dc.l	$12345678
am2rng_r:
		movea.l	(4).w,a6        ; AbsExecBase
		movea.l	$0114(a6),a2    ; ThisTask
		tst.l	$00AC(a2)       ; pr_CLI
		bne.b	.open
		lea	$005C(a2),a0    ; pr_MsgPort
		jsr	-$0180(a6)      ; _LVOWaitPort
		lea	$005C(a2),a0    ; pr_MsgPort
		jsr	-$0174(a6)      ; _LVOGetMsg
		jsr	-$0084(a6)      ; _LVOForbid (preserves D0)
		movea.l	d0,a1
		jsr	-$017A(a6)      ; _LVOReplyMsg
.fail:
		moveq	#20,d0          ; RETURN_FAIL
		rts
.open:
		lea	.name(pc),a1
		moveq	#36,d0          ; V36 for VPrintf
		jsr	-$0228(a6)      ; _LVOOpenLibrary
		tst.l	d0
		beq.b	.fail
		movea.l	d0,a6
		subq.l	#8,sp
		clr.l	(sp)
		move.w	#32767-1,d3
.loop:
		bsr.b	AM2Random
		move.l	d0,$0004(sp)
		addq.l	#1,(sp)
		pea	.line(pc)
		move.l	(sp)+,d1
		move.l	sp,d2
		jsr	-$03BA(a6)      ; _LVOVPrintf
		dbra	d3,.loop
		addq.l	#8,sp
		movea.l	a6,a1
		movea.l	(4).w,a6        ; AbsExecBase
		jsr	-$019E(a6)      ; _LVOCloseLibrary
		moveq	#0,d0           ; RETURN_OK
		rts
.name:
		dc.b	"dos.library",0
.line:
		dc.b	"%lu: %lu",10,0
		dc.b	"https://gitlab.com/ambermoon/research"
	align	2
