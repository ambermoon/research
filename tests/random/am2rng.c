#include <stdint.h>

uint_least32_t AM2Random(void)
{
  static uint_least16_t state[2] = {UINT16_C(0x1234), UINT16_C(0x5678)};
  uint_least16_t const result = state[1];
  state[1] ^= (uint_least16_t)(
      (uint_least16_t)(
          /* std::rotr<std::uint16_t>(state[0], 1) */
          (uint_least16_t)((uint_least16_t)(state[0] >> 1U) |
                           (uint_least16_t)((uint_least16_t)(state[0] << 15U) &
                                            (uint_least16_t)UINT16_C(0xFFFF))) +
          (uint_least16_t)UINT16_C(7)) &
      (uint_least16_t)UINT16_C(0xFFFF));
  state[0] = result;
  return result;
}

#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(AM2RNG_STDOUT_SETMODE) && (AM2RNG_STDOUT_SETMODE)
#include <fcntl.h>
#include <io.h>
#endif

int main(int argc, char *argv[])
{
  if (2 == argc) {
    switch (argv[1][0]) {
    case 'a':
      while (printf("%" PRIuLEAST32 ",", AM2Random()) > 0)
        continue;
      return EXIT_FAILURE;
    case 'b':
#if defined(AM2RNG_STDOUT_SETMODE) && (AM2RNG_STDOUT_SETMODE)
      _setmode(_fileno(stdout), _O_BINARY);
#endif
      for (;;) {
        uint32_t data = AM2Random() | (AM2Random() << 16U);
        if (0U >= fwrite(&data, 32U / CHAR_BIT, 1U, stdout))
          return EXIT_FAILURE;
      }
    case 'r':
      for (int i = 0; i < 32767; ++i)
        printf("%d: %" PRIuLEAST32 "\n", i + 1, AM2Random());
      return EXIT_SUCCESS;
    }
  }
  fprintf(stderr, "Usage: am2rng a[scii] | b[inary] | r[eference]\n");
  return EXIT_FAILURE;
}
