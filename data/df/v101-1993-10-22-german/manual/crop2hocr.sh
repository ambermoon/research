#!/bin/sh

main() {
  main_tess_root='./crop'
  main_file_list='../manual_scan.txt'
  main_word_list='../manual_words.txt'
  main_hocr_base='../scan/Manual'
  [ -d "${main_tess_root}" ] || exit 1
  cd "${main_tess_root}"
  [ -f "${main_file_list}" ] || exit 1
  [ -f "${main_word_list}" ] || exit 1
  command -p tesseract "${main_file_list}" "${main_hocr_base}" \
    -l deu \
    --user-words "${main_word_list}" \
    --dpi 300 \
    --psm 11 \
    -c 'tessedit_char_blacklist=%_}—’‚”„' \
    -c 'document_title=Ambermoon' \
    "$@" \
    hocr
}

main "$@"
