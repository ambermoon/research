#!/bin/sh

img2pdf --output "./manual_scan.pdf" \
 --title "Ambermoon" \
 --author "Thalion Software" \
 --producer "" \
 --nodate \
 --viewer-panes outlines \
 --viewer-page-layout twopageright \
 --pagesize 210mmx190mm \
 --imgsize 300dpi \
 -- \
 "./scan/Manual000.jpg" \
 "./scan/Manual001.png" "./scan/Manual002.png" \
 "./scan/Manual003.png" "./scan/Manual004.png" \
 "./scan/Manual005.png" "./scan/Manual006.png" \
 "./scan/Manual007.png" "./scan/Manual008.png" \
 "./scan/Manual009.png" "./scan/Manual010.png" \
 "./scan/Manual011.png" "./scan/Manual012.png" \
 "./scan/Manual013.png" "./scan/Manual014.png" \
 "./scan/Manual015.png" "./scan/Manual016.png" \
 "./scan/Manual017.png" "./scan/Manual018.png" \
 "./scan/Manual019.png" "./scan/Manual020.png" \
 "./scan/Manual021.png" "./scan/Manual022.png" \
 "./scan/Manual023.png" "./scan/Manual024.png" \
 "./scan/Manual025.png" "./scan/Manual026.png" \
 "./scan/Manual027.png" "./scan/Manual028.png" \
 "./scan/Manual029.png" "./scan/Manual030.png" \
 "./scan/Manual031.png" "./scan/Manual032.png" \
 "./scan/Manual033.png" "./scan/Manual034.png" \
 "./scan/Manual035.png" "./scan/Manual036.png" \
 "./scan/Manual037.png" "./scan/Manual038.png" \
 "./scan/Manual039.png" "./scan/Manual040.png" \
 "./scan/Manual041.png" "./scan/Manual042.png" \
 "./scan/Manual043.png" "./scan/Manual044.png" \
 "./scan/Manual045.png" "./scan/Manual046.png" \
 "./scan/Manual047.png" "./scan/Manual048.png" \
 "./scan/Manual049.png" "./scan/Manual050.png" \
 "./scan/Manual051.png" "./scan/Manual052.png" \
 "./scan/Manual053.png" "./scan/Manual054.png" \
 "./scan/Manual055.png" "./scan/Manual056.png" \
 "./scan/Manual057.png" "./scan/Manual058.png" \
 "./scan/Manual059.png" "./scan/Manual060.png" \
 "./scan/Manual061.png" "./scan/Manual062.png" \
 "./scan/Manual063.png" "./scan/Manual064.png" \
 "./scan/Manual065.png" "./scan/Manual066.png" \
 "./scan/Manual067.png" "./scan/Manual068.png" \
 "./scan/Manual069.png" "./scan/Manual070.png" \
 "./scan/Manual071.png" "./scan/Manual072.png" \
 "./scan/Manual073.png" "./scan/Manual074.png" \
 "./scan/Manual075.png" "./scan/Manual076.png" \
 "./scan/Manual077.png" "./scan/Manual078.png" \
 "./scan/Manual079.png" "./scan/Manual080.png" \
 "./scan/Manual081.png" "./scan/Manual082.png" \
 "./scan/Manual083.png" "./scan/Manual084.png" \
 "./scan/Manual085.png" "./scan/Manual086.png" \
 "./scan/Manual087.png" "./scan/Manual088.png" \
 "./scan/Manual089.png" "./scan/Manual090.png" \
 "./scan/Manual091.png" "./scan/Manual092.png" \
 "./scan/Manual093.png" "./scan/Manual094.png" \
 "./scan/Manual095.png" "./scan/Manual096.png" \
 "./scan/Manual097.png" "./scan/Manual098.png" \
 "./scan/Manual099.png" "./scan/Manual100.png" \
 "./scan/Manual101.png" "./scan/Manual102.png" \
 "./scan/Manual103.png" "./scan/Manual104.png" \
 "./scan/Manual105.png" "./scan/Manual106.png" \
 "./scan/Manual107.png" "./scan/Manual108.png" \
 "./scan/Manual109.png" "./scan/Manual110.png" \
 "./scan/Manual111.png" "./scan/Manual112.png" \
 "./scan/Manual113.png" "./scan/Manual114.png" \
 "./scan/Manual115.png" "./scan/Manual116.png" \
 "./scan/Manual117.png" "./scan/Manual118.png" \
 "./scan/Manual119.jpg" \
 && \
pdftk \
 "./manual_scan.pdf" dump_data output \
 "./manual_data.info" \
 && \
cat \
 "./manual_bookmark_data.info" \
 "./manual_pagelabel_data.info" >> \
 "./manual_data.info" \
 && \
pdftk \
 "./manual_scan.pdf" update_info \
 "./manual_data.info" output \
 "./manual_info.pdf" \
 && rm "./manual_scan.pdf" "./manual_data.info" && \
qpdf \
 --linearize \
 --newline-before-endstream \
 --deterministic-id \
 --keep-inline-images \
 "./manual_info.pdf" \
 "./manual.pdf" \
 && rm "./manual_info.pdf"

