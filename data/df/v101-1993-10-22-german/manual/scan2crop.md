| filename      | layout  | text rect(s)                                        | image rect(s)                      |
| :------------ | :------ | :-------------------------------------------------- | :--------------------------------- |
| Manual000.jpg | front   |                                                     | 1620x548+451+100 1065x1591+720+489 |
| Manual001.png | empty   |                                                     |                                    |
| Manual002.png | chapter | 395x413+1096+920                                    |                                    |
| Manual003.png | left    | 1785x1384+463+526                                   |                                    |
| Manual004.png | right   | 1779x1408+291+524                                   |                                    |
| Manual005.png | empty   |                                                     |                                    |
| Manual006.png | chapter | 395x413+1096+920                                    |                                    |
| Manual007.png | left    | 1785x1407+463+526                                   |                                    |
| Manual008.png | right   | 1779x1408+291+524                                   |                                    |
| Manual009.png | left    | 1785x1375+463+526                                   |                                    |
| Manual010.png | right   | 1779x1408+291+524                                   |                                    |
| Manual011.png | last    | 1946x1475+266+525                                   |                                    |
| Manual012.png | chapter | 395x413+1096+920                                    |                                    |
| Manual013.png | left    | 1785x1407+463+526                                   |                                    |
| Manual014.png | right   | 1779x1408+291+524                                   |                                    |
| Manual015.png | left    | 1785x1376+463+526                                   |                                    |
| Manual016.png | right   | 1779x1408+291+524                                   |                                    |
| Manual017.png | left    | 1785x1378+463+526                                   |                                    |
| Manual018.png | right   | 1779x1408+291+524                                   |                                    |
| Manual019.png | left    | 1785x1378+463+526                                   |                                    |
| Manual020.png | right   | 1779x1408+291+524                                   |                                    |
| Manual021.png | left    | 1785x1394+463+526                                   |                                    |
| Manual022.png | right   | 1779x1408+291+524                                   |                                    |
| Manual023.png | left    | 1781x1392+467+526                                   |                                    |
| Manual024.png | right   | 1779x1408+291+524                                   |                                    |
| Manual025.png | left    | 1785x170+463+526 761x712+1487+696 1785x493+463+1408 | 1018x712+469+696                   |
| Manual026.png | right   | 1779x105+291+524 758x714+291+629 1779x589+291+1343  | 1019x714+1049+629                  |
| Manual027.png | left    | 1785x1378+463+526 1731x29+517+1904                  |                                    |
| Manual028.png | right   | 758x722+291+524 1779x686+291+1246                   | 1017x714+1049+532                  |
| Manual029.png | empty   |                                                     |                                    |
| Manual030.png | chapter | 395x413+1096+920                                    |                                    |
| Manual031.png | left    | 1785x1380+463+526                                   |                                    |
| Manual032.png | right   | 1779x567+291+524 761x841+291+1091                   | 1018x712+1052+1091                 |
| Manual033.png | left    | 1785x591+463+526 757x816+1491+1117                  | 1021x712+470+1117                  |
| Manual034.png | right   | 1779x1408+291+524                                   |                                    |
| Manual035.png | left    | 1781x1407+467+526                                   |                                    |
| Manual036.png | right   | 1779x1408+291+524                                   |                                    |
| Manual037.png | empty   |                                                     |                                    |
| Manual038.png | chapter | 395x413+1096+920                                    |                                    |
| Manual039.png | left    | 753x721+1495+526 1785x686+463+1247                  | 1021x716+474+531                   |
| Manual040.png | right   | 754x722+291+524 1779x686+291+1246                   | 1020x711+1045+535                  |
| Manual041.png | left    | 758x719+1490+526 1785x658+463+1245                  | 1020x711+470+534                   |
| Manual042.png | right   | 755x727+291+524 1779x681+291+1251                   | 1022x711+1046+540                  |
| Manual043.png | left    | 1785x573+463+526 698x807+1550+1099                  | 1019x713+531+1099                  |
| Manual044.png | right   | 1779x413+291+524 755x712+291+937 1779x283+291+1649  | 1019x712+1046+937                  |
| Manual045.png | left    | 1785x1391+463+526                                   |                                    |
| Manual046.png | right   | 1779x524+291+524 712x711+291+1048 1779x173+291+1759 | 1019x711+1003+1048                 |
| Manual047.png | left    | 1785x1380+463+526                                   |                                    |
| Manual048.png | right   | 1779x1408+291+524                                   |                                    |
| Manual049.png | empty   |                                                     |                                    |
| Manual050.png | chapter | 395x413+1096+920 4x9+1491+1103                      |                                    |
| Manual051.png | left    | 1785x231+463+526 756x713+1492+757 1785x437+463+1470 | 1020x713+472+757                   |
| Manual052.png | right   | 1779x1408+291+524                                   |                                    |
| Manual053.png | left    | 1785x1407+463+526                                   |                                    |
| Manual054.png | right   | 1779x1408+291+524                                   |                                    |
| Manual055.png | left    | 1785x1376+463+526                                   |                                    |
| Manual056.png | right   | 1779x1408+291+524                                   |                                    |
| Manual057.png | empty   |                                                     |                                    |
| Manual058.png | chapter | 395x413+1096+920                                    |                                    |
| Manual059.png | left    | 760x722+1488+526 1785x656+463+1248                  | 1018x711+470+537                   |
| Manual060.png | right   | 1779x1408+291+524                                   |                                    |
| Manual061.png | last    | 1946x1475+266+525                                   |                                    |
| Manual062.png | chapter | 395x413+1096+920                                    |                                    |
| Manual063.png | left    | 1782x1407+466+526                                   |                                    |
| Manual064.png | right   | 1779x196+291+524 759x712+291+720 1779x500+291+1432  | 1017x712+1050+720                  |
| Manual065.png | left    | 1785x648+463+526 760x759+1488+1174                  | 1020x714+468+1174                  |
| Manual066.png | right   | 1779x1408+291+524                                   |                                    |
| Manual067.png | left    | 1785x279+463+526 756x713+1492+805 1783x415+465+1518 | 1019x713+473+805                   |
| Manual068.png | right   | 1779x1408+291+524                                   |                                    |
| Manual069.png | left    | 1785x673+463+526 752x734+1496+1199                  | 1019x712+477+1199                  |
| Manual070.png | right   | 1779x1408+291+524                                   |                                    |
| Manual071.png | left    | 1785x589+463+526 751x818+1497+1115                  | 1018x713+479+1115                  |
| Manual072.png | right   | 1779x1408+291+524                                   |                                    |
| Manual073.png | last    | 1946x1475+266+525                                   |                                    |
| Manual074.png | chapter | 395x413+1096+920                                    |                                    |
| Manual075.png | left    | 1785x422+463+526 758x714+1490+948 1778x252+470+1662 | 1020x714+470+948                   |
| Manual076.png | right   | 1779x1408+291+524                                   |                                    |
| Manual077.png | left    | 1778x1407+470+526                                   |                                    |
| Manual078.png | right   | 1779x573+291+524 757x835+291+1097                   | 1020x712+1048+1097                 |
| Manual079.png | left    | 1785x1377+463+526                                   |                                    |
| Manual080.png | right   | 1779x344+291+524 725x712+291+868 1779x352+291+1580  | 1019x712+1016+868                  |
| Manual081.png | left    | 1785x560+463+526 758x847+1490+1086                  | 1018x712+472+1086                  |
| Manual082.png | right   | 1779x360+291+524 749x711+291+884 1779x337+291+1595  | 1019x711+1040+884                  |
| Manual083.png | left    | 1785x563+463+526 764x844+1484+1089                  | 1021x714+463+1089                  |
| Manual084.png | right   | 1779x1408+291+524                                   |                                    |
| Manual085.png | left    | 1785x512+463+526 752x895+1496+1038                  | 1020x717+476+1038                  |
| Manual086.png | right   | 753x793+291+524 1779x615+291+1317                   | 1019x714+1044+603                  |
| Manual087.png | left    | 1785x1376+463+526                                   |                                    |
| Manual088.png | right   | 1779x578+291+524 755x830+291+1102                   | 1017x711+1047+1102                 |
| Manual089.png | empty   |                                                     |                                    |
| Manual090.png | chapter | 395x413+1096+920                                    |                                    |
| Manual091.png | left    | 1785x331+463+526 751x711+1497+857 1782x365+466+1568 | 1019x711+478+857                   |
| Manual092.png | right   | 1779x1408+291+524                                   |                                    |
| Manual093.png | left    | 1785x1375+463+526                                   |                                    |
| Manual094.png | right   | 1779x1408+291+524                                   |                                    |
| Manual095.png | left    | 1785x670+463+526 754x737+1494+1196                  | 1019x713+475+1196                  |
| Manual096.png | right   | 1779x1408+291+524                                   |                                    |
| Manual097.png | last    | 1946x1475+266+525                                   |                                    |
| Manual098.png | chapter | 395x413+1096+920                                    |                                    |
| Manual099.png | last    | 1946x1475+266+525                                   |                                    |
| Manual100.png | chapter | 395x413+1096+920                                    |                                    |
| Manual101.png | left    | 1785x1381+463+526                                   |                                    |
| Manual102.png | right   | 1779x1408+291+524                                   |                                    |
| Manual103.png | left    | 1785x1380+463+526                                   |                                    |
| Manual104.png | right   | 1779x1408+291+524                                   |                                    |
| Manual105.png | left    | 1785x1378+463+526                                   |                                    |
| Manual106.png | right   | 1779x1408+291+524                                   |                                    |
| Manual107.png | left    | 1785x1382+463+526 1737x25+511+1908                  |                                    |
| Manual108.png | right   | 1779x1408+291+524                                   |                                    |
| Manual109.png | left    | 1785x1381+463+526                                   |                                    |
| Manual110.png | right   | 1779x1408+291+524                                   |                                    |
| Manual111.png | left    | 1785x1371+463+526                                   |                                    |
| Manual112.png | right   | 1779x1408+291+524                                   |                                    |
| Manual113.png | left    | 1785x1407+463+526                                   |                                    |
| Manual114.png | right   | 1758x1408+291+524                                   |                                    |
| Manual115.png | empty   |                                                     |                                    |
| Manual116.png | empty   |                                                     |                                    |
| Manual117.png | empty   |                                                     |                                    |
| Manual118.png | empty   |                                                     |                                    |
| Manual119.jpg | back    | 434x38+1024+1452                                    | 947x793+767+183 343x265+1069+1121  |

