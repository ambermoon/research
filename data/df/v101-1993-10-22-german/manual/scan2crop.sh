#!/bin/sh

trim() {
  for trim_var in "$@" ; do
    [ -n "${trim_var}" ] || continue
    eval "trim_str=\"\${${trim_var}}\""
    [ -n "${trim_str}" ] || continue
    trim_str="${trim_str#"${trim_str%%[![:space:]]*}"}"
    trim_str="${trim_str%"${trim_str##*[![:space:]]}"}"
    eval "${trim_var}=\"\${trim_str}\""
  done
}

scan() {
  scan_line_var="$1" ; shift
  [ -n "${scan_line_var}" ] && eval "scan_line_str=\"\${${scan_line_var}}\"" || scan_line_str=''
  for scan_item_var in "$@" ; do
    scan_item_str="${scan_line_str%%[[:space:]]*}"
    scan_line_str="${scan_line_str#"${scan_item_str}"}"
    scan_line_str="${scan_line_str#"${scan_line_str%%[![:space:]]*}"}"
    [ -z "${scan_item_var}" ] || eval "${scan_item_var}=\"\${scan_item_str}\""
  done
  [ -z "${scan_line_var}" ] || eval "${scan_line_var}=\"\${scan_line_str}\""
}

quote() {
  printf "%s\n" "$1" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/'/"
}

main() {
  main_scan_root='./scan'
  main_crop_base="${main_scan_root}/Manual001.png"
  main_crop_root='./crop'
  [ -d "${main_scan_root}" ] || exit 1
  [ -f "${main_crop_base}" ] || exit 1
  [ -d "${main_crop_root}" ] || mkdir -p "${main_crop_root}"
  while IFS='|' read -r _ main_file_name main_page_type main_text_list main_draw_list _ ; do
    trim main_file_name main_page_type main_text_list main_draw_list
    main_scan_file="${main_scan_root}/${main_file_name%.*}.png"
    [ -f "${main_scan_file}" ] || continue
    main_draw_args=''
    while [ -n "${main_draw_list}" ] ; do
      scan main_draw_list main_draw_rect
      [ -n "${main_draw_rect}" ] || continue
      IFS='x+' read -r main_draw_w main_draw_h main_draw_x0 main_draw_y0 _ <<EOF
${main_draw_rect}
EOF
      main_draw_args="${main_draw_args}-draw $(quote "rectangle ${main_draw_x0},${main_draw_y0} $((${main_draw_x0} + ${main_draw_w})),$((${main_draw_y0} + ${main_draw_h}))") "
    done
    main_draw_args="${main_draw_args:+"-fill black ${main_draw_args}"}"
    main_crop_args=''
    while [ -n "${main_text_list}" ] ; do
      scan main_text_list main_text_rect
      [ -n "${main_text_rect}" ] || continue
      main_crop_args="${main_crop_args}\( $(quote "${main_scan_file}")"
      [ "${main_page_type}" != "back" ] || main_crop_args="${main_crop_args} -negate"
      main_crop_args="${main_crop_args} -crop $(quote "${main_text_rect}") \) "
    done
    main_crop_args="${main_crop_args:+"${main_crop_args} -layers flatten"}"
    main_crop_file="${main_crop_root}/${main_file_name}"
    eval "set -- ${main_draw_args}${main_crop_args}"
    command -p convert -units PixelsPerInch -density 300 "${main_crop_base}" \
      "$@" \
      -level 20%,100% \
      -quality 100 \
      "${main_crop_file}" \
    && echo "${main_crop_file}"
  done < './scan2crop.md'
}

main "$@"
