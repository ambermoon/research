For PDF/A-1 compatibility the V4 color profile `scan-color.icc` has to be 
converted to a V2 color profile.

In the first step, this has been done by using the internal GIMP function 
[gimp_color_profile_new_rgb_srgb_internal][gimpcolorprofile-c] with an 
additional `cmsSetProfileVersion(profile, 2.3)` (the used `chromaticityTag`
has been introduced with the addendum `ICC.1A:1999-04` to `ICC.1:1998-09` - 
both [PDF/A TechNote 0002][pdfa-technote-0002] 
and [PDF/A TechNote 0010][pdfa-technote-0010] 
clarify that conforming readers shall process any 2.x profile), 
and saved with `cmsSaveProfileToFile(profile, "scan-color-v2.icc")`.

Secondly, as the saved color profile was not fully conformant 
(padding included in the element size of some tag table entries),  
  - converted the color profile from ICC into XML format with the 
   `iccToXml` tool from the [DemoIccMAX project][demoiccmax-project]
  - changed the creation timestamp in the XML to the timestamp of 
    [commit 05c5ca32][gimpcommit05c5ca32] that introduced the 
    current `GIMP built-in sRGB` parameters that are in still in use
  - used the `iccFromXML` tool to convert the modified XML into ICC


[gimpcolorprofile-c]: https://gitlab.gnome.org/GNOME/gimp/-/blob/master/libgimpcolor/gimpcolorprofile.c
[pdfa-technote-0002]: https://www.pdfa.org/resource/technical-note-tn0002-color-in-pdfa-1/
[pdfa-technote-0010]: https://www.pdfa.org/resource/technote-0010-clarifications-of-iso-19005-parts-1-3-for-developers-of-pdfa-creators-and-validators/
[demoiccmax-project]: https://github.com/InternationalColorConsortium/DemoIccMAX
[gimpcommit05c5ca32]: https://gitlab.gnome.org/GNOME/gimp/-/commit/05c5ca32160f37b9a36b07f465bb834dc8af27c3
