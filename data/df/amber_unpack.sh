#!/bin/sh
for v in \
  ./v099-1993-08-25-german-pdy \
  ./v101-1993-10-22-german \
  ./v104-1993-11-18-german \
  ./v107-1994-03-26-english
do
  for n in A B C D E F G H I J
  do
    b="${v}/AMBER_${n}"
    for x in .blkdev .bootcode .xdfmeta
    do
      [ -f  "${b}${x}" ] || continue
      rm -f "${b}${x}"
    done
    [ -d "${b}" ] || continue
    rm -rf "${b}"
  done
  [ ! "${1:-""}" = "clean" ] || continue
  for n in a b c d e f g h i j
  do
    a="${v}/amber_${n}.adf"
    [ -f "${a}" ] || continue
    echo "${a}"
    xdftool "${a}" unpack "${v}" fsuae
  done
done
