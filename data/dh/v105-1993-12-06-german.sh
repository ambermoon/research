#!/bin/sh
[ -d ./v104-1993-11-18-german ] && {
  [ -d ./v105-1993-12-06-german ] || {
    mkdir ./v105-1993-12-06-german && {
      cp -R ./v104-1993-11-18-german/* ./v105-1993-12-06-german/ && \
      cp -R ../patch/v105-1993-12-06-german/* ./v105-1993-12-06-german/ || {
        rm -rf ./v105-1993-12-06-german
        return 1
      }
    }
  }
}
