#!/bin/sh
[ -d ./lhafsuae ] || unzip lhafsuae.zip
for v in \
  v101-1993-10-22-german \
  v104-1993-11-18-german \
  v105-1993-12-06-german \
  v107-1994-03-26-english
do
  [   -d ./${v}               ] || continue
  [ ! -f ./Ambermoon-${v}.lha ] || continue
  rm  -f          ./lhafsuae/LhA/archive.lha
  rm -rf          ./lhafsuae/LhA/archive
  mkdir           ./lhafsuae/LhA/archive
  cp -R  ./${v}/* ./lhafsuae/LhA/archive/
  for n in 01 02 03 04 05 06 07 08 09 10; do
    cp            ./lhafsuae/LhA/archive/Amberfiles/Save.${n}.uaem \
                  ./lhafsuae/LhA/archive/Amberfiles/Save.${n}/.info.uaem
  # rm            ./lhafsuae/LhA/archive/Amberfiles/Save.${n}/.info
  done
  rm -rf          ./lhafsuae/LhA/archive/S
  command -p fs-uae ./lhafsuae.fs-uae
  mv              ./lhafsuae/LhA/archive.lha  ./Ambermoon-${v}.lha
# mv              ./lhafsuae/LhA/archive.list ./Ambermoon-${v}.list
  rm  -f          ./lhafsuae/LhA/archive.lha.uaem
  rm  -f          ./lhafsuae/LhA/archive.list
  rm  -f          ./lhafsuae/LhA/archive.list.uaem
done

