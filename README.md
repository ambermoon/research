Welcome to my research repository for Ambermoon.

In the meantime there are much more advanced projects from Pyrdacor:  
https://github.com/Pyrdacor/Ambermoon and  
https://github.com/Pyrdacor/Ambermoon.net

I'm still working on this project as much as my free time allows.  
However, I am currently pretty busy setting up a
[Ghidra server](https://gitlab.com/ambermoon/research/-/wikis/ghidra).

Project Wiki: https://gitlab.com/ambermoon/research/-/wikis/home
